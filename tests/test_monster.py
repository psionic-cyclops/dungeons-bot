import pytest

from src import dnd
# from src import util
from src.monster import Monster

N = 10


@pytest.fixture
def monster() -> Monster:

    monster_dict = dnd.get_monster("aboleth")
    return Monster(monster_dict)


def test_init(monster: Monster) -> None:

    assert monster._hitpoints == monster._max_hitpoints
    assert monster._xp == monster._monster["xp"]
    assert monster._challenge_rating == monster._monster["challenge_rating"]

    if "image" in monster._monster:
        assert monster._image_url.endswith(monster._monster["image"])
    else:
        assert len(monster._image_url) == 0

    assert monster._abilities == {
        "str": monster._monster["strength"],
        "dex": monster._monster["dexterity"],
        "con": monster._monster["constitution"],
        "int": monster._monster["intelligence"],
        "wis": monster._monster["wisdom"],
        "cha": monster._monster["charisma"]
    }
    assert len(monster._saving_throws) == len(monster._abilities)
    assert len(monster._skills) > 0

    assert monster._proficiency_bonus == dnd.cr_proficiency(monster._challenge_rating)


def test_armor_class(monster: Monster) -> None:

    assert monster.armor_class() >= 0


def test_emoji(monster: Monster) -> None:

    assert isinstance(monster.emoji(), str)


def test_short_bio(monster: Monster) -> None:

    assert len(monster.short_bio()) > 0


def test_bio(monster: Monster) -> None:

    assert len(monster.bio()) > 0


def test_bio_extended(monster: Monster) -> None:

    assert len(monster.extended_bio()) > 0


def test_attack(monster: Monster) -> None:

    for _ in range(len(monster._monster["actions"])):
        attack = monster.attack(monster)

        if attack.attack_hit.value <= 0:
            assert attack.damage == 0
        else:
            assert attack.damage > 0

    for _ in range(2):
        for action in monster._monster["actions"]:
            attack = monster.attack(monster, action)

            assert attack.name == action["name"]

            if attack.attack_hit.value <= 0:
                assert attack.damage == 0
            else:
                assert attack.damage > 0


def test_all_monsters() -> None:

    monsters = dnd.get_monsters()

    for m in monsters:
        monster = Monster(dnd.get_monster(m["index"]))
        test_init(monster)
        test_armor_class(monster)
        test_emoji(monster)
        test_short_bio(monster)
        test_bio(monster)
        test_attack(monster)
