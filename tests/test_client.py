from src import client
from src import util


# TODO: perhaps do proper mocking here for Mastodon

def test_command_reply() -> None:

    client.dry = True
    status = client.post_status("test")
    assert "id" in client.command_reply("test reply", status)


def test_post_status() -> None:

    client.dry = True

    assert "id" in client.post_status("Test status", "Last result")
    assert "id" in client.post_status("Test status", "Last result", False)


def test_post_poll() -> None:

    client.dry = True

    status = client.post_poll("Test status", "Last result", ["Option 1", "Option 2"])
    assert "id" in status
    assert "poll" in status
    assert client.status_chain == status["id"]

    status = client.post_poll("Test status", "Last result", ["Option 1", "Option 2"], False)
    assert "id" in status
    assert "poll" in status
    assert client.status_chain == status["id"]

    status = client.post_poll("Test status", "Last result", ["Option 1", "Option 2"], extend=True)
    assert "id" in status
    assert "poll" in status
    assert client.status_chain == status["id"]

    status = client.post_poll("Test status", "Last result", ["Option 1", "Option 2"], False, True)
    assert "id" in status
    assert "poll" in status
    assert client.status_chain == status["id"]


def test_delete_post() -> None:

    client.dry = True

    status = client.post_status("Test status", "Last result")
    assert client.delete_post(status["id"])


def test_poll_result() -> None:

    client.dry = True

    options = ["Option 1", "Option 2"]
    status = client.post_poll("Test status", "Last result", options)
    result = client.poll_result(status["id"])
    assert 0 <= result < len(options)


def test_find_winning_index() -> None:

    votes = [1, 2, 3, 5]
    assert client.find_winning_index(votes) == 3

    votes = [1, 2, 3, 5]
    groups = [1, 2]
    assert client.find_winning_index(votes, groups) == 2

    votes = [1, 2, 3, 5]
    groups = [[0, 1, 2], [3]]
    assert client.find_winning_index(votes, groups) == 2

    votes = [1, 2, 3, 5]
    groups = [[0, 1], [2, 3]]
    assert client.find_winning_index(votes, groups) == 3

    votes = [1, 2, 3, 5]
    groups = [[0], [1], [2], [3]]
    assert client.find_winning_index(votes, groups) == 3

    votes = [1, 1, 0, 2]
    groups = [[0, 1, 2], [3]]
    assert client.find_winning_index(votes, groups) == 0

    votes = [1, 1, 0, 3]
    groups = [[0, 1, 2], [3]]
    assert client.find_winning_index(votes, groups) == 3

    votes = [1, 1, 0, 1]
    groups = [[2, 3], [0], [1]]
    assert client.find_winning_index(votes, util.sort_multi(groups)) == 0

    votes = [19, 1, 13, 7]
    groups = [[2, 3], [0], [1]]
    assert client.find_winning_index(votes, util.sort_multi(groups)) == 2


def test_get_followers() -> None:

    client.dry = True

    followers = client.get_followers()
    assert len(followers) > 0

    for follower in followers:
        assert isinstance(follower, str)
        assert len(follower) > 0


def test_update_profile() -> None:

    client.dry = True

    assert "note" in client.update_profile("Bio")
