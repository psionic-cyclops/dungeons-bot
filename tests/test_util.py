from src import util
from src import client


def test_globals() -> None:

    assert bool(util.logger)
    root = util.root
    assert len(root) > 0
    emojis = util.emojis
    assert len(emojis) > 0
    config = util.config
    assert len(config) > 0
    tips = util.tips
    assert len(tips) > 0

    tip_indices = set()
    for tip in util.tips:
        assert len(util.tips[tip]) < client.STATUS_LIMIT - len(client.HASHTAG_TIP) - len(client.CW_TIP)
        assert tip not in tip_indices
        tip_indices.add(tip)


def test_format_duration() -> None:

    # Minutes
    assert "00:00" == util.format_duration(0)
    assert "00:00" == util.format_duration(1)

    assert "00:01" == util.format_duration(60)
    assert "00:01" == util.format_duration(119)

    # Hours
    assert "01:00" == util.format_duration(3600)
    assert "01:59" == util.format_duration(7199)

    # Days
    assert "1 day, 00:00" == util.format_duration(86400)
    assert "1 day, 01:59" == util.format_duration(93599)

    assert "2 days, 00:00" == util.format_duration(172800)


def test_sum_array() -> None:

    assert util.sum_array([1, 2, 3, 4]) == 10
    assert util.sum_array([1, 2, [3, 4]]) == 10
    assert util.sum_array([[1, 2], 3, 4]) == 10
    assert util.sum_array([[1, 2], [3, 4]]) == 10


def test_multi_sort() -> None:

    assert util.sort_multi([3, 2, 1, 0]) == [0, 1, 2, 3]
    assert util.sort_multi([0, 1, 2, 3]) == [0, 1, 2, 3]

    assert util.sort_multi([[3, 2], [0], [5, 4, 6]]) == [[0], [2, 3], [4, 5, 6]]


def test_int_in_array() -> None:

    array = [10, 9, [8, 7, [6, 99, 5]], [4, 3], 2, [1]]
    assert util.int_in_array(array, 99)


def test_float_to_frac() -> None:

    assert util.float_to_frac(0) == "0"
    assert util.float_to_frac(0.125) == "1/8"
    assert util.float_to_frac(0.25) == "1/4"
    assert util.float_to_frac(0.5) == "1/2"
    assert util.float_to_frac(1) == "1"
    assert util.float_to_frac(20) == "20"


def test_key_or_less() -> None:

    assert util.key_or_less("5", {"1": None, "5": None, "9": None}) == "5"
    assert util.key_or_less("4", {"1": None, "5": None, "9": None}) == "1"
    assert util.key_or_less("5", {"1": None, "5": None, "9": None}) == "5"
    assert util.key_or_less("2", {"3": None, "5": None, "9": None}) == "3"
    assert util.key_or_less("10", {"3": None, "5": None, "9": None}) == "9"