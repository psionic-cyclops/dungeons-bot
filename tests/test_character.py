import pytest
import re

from src.character import Character, client
from src import dnd


@pytest.fixture
def character() -> Character:

    client.dry = True
    return Character("test")


@pytest.fixture
def character_init(character: Character) -> Character:

    character.init()
    return character


def test_init(character: Character) -> None:

    # __init__
    assert character._name.startswith("Test")
    assert len(character._race) > 0
    assert len(character._class) > 0

    assert character._level == 1
    assert character._xp == 0
    assert character._hit_dice == character._level

    assert character._proficiency_bonus == 2

    # init
    character.init()

    for ability in character._abilities:
        assert character._abilities[ability] >= 3
        assert character._abilities[ability] <= 20

    assert len(character._proficiencies) > 0
    assert len(character._hands) > 0
    assert character._armor == {} or len(character._armor) > 0
    assert 30 <= character._gp <= 120

    assert character._max_hitpoints > 0
    assert character._hitpoints == character._max_hitpoints


def test_ability_priority(character_init: Character) -> None:

    assert len(character_init.ability_priority()) > 0
    assert len(character_init.ability_priority()) <= len(character_init.ORDERED_ABILITY_INDEXES)
    for ability in character_init.ability_priority():
        assert ability in character_init.ORDERED_ABILITY_INDEXES


def test_proficient_weapons(character_init: Character) -> None:

    weapons = character_init.proficient_weapons()
    assert len(weapons) > 0


def test_proficient_armor(character_init: Character) -> None:

    armor = character_init.proficient_armor()
    assert armor == [] or len(armor) > 0


def test_armor_class(character_init: Character) -> None:

    character_init.unequip()

    ac = character_init.armor_class(dnd.get_equipment("leather-armor"))
    assert ac == 11 + character_init.ability_modifier("dex")

    ac = character_init.armor_class(dnd.get_equipment("chain-mail"))
    assert ac == 16


def test_has_feature(character_init: Character) -> None:

    character_init._features.append(dnd.get_feature("divine-sense"))
    assert character_init.has_feature("divine-sense")


def test_weapon_modifier(character_init: Character) -> None:

    club = dnd.get_equipment("club")
    modifier = character_init.weapon_modifier(club)
    assert modifier == character_init.ability_modifier("str")

    crossbow = dnd.get_equipment("crossbow-light")
    modifier = character_init.weapon_modifier(crossbow)
    assert modifier == character_init.ability_modifier("dex")


def test_spend(character_init: Character) -> None:

    gold = character_init._gp
    assert character_init.spend(1) == gold - 1
    assert character_init._gp == gold - 1


def test_earn(character_init: Character) -> None:

    gold = character_init._gp
    assert character_init.earn(1) == gold + 1
    assert character_init._gp == gold + 1


def test_equip(character_init: Character) -> None:

    character_init.unequip()
    assert len(character_init._hands) == 0
    assert len(character_init._armor) == 0

    leather_armor = dnd.get_equipment("leather-armor")
    dagger = dnd.get_equipment("dagger") # light 1d4
    shortsword = dnd.get_equipment("shortsword") # light 1d6
    mace = dnd.get_equipment("mace") # one-handed 1d6
    longsword = dnd.get_equipment("longsword") # one-handed 1d8
    greataxe = dnd.get_equipment("greataxe") # two-handed
    shield = dnd.get_equipment("shield") # shield

    assert character_init.equip(dagger) == []
    assert character_init._hands == [dagger]

    assert character_init.equip(shortsword) == []
    assert character_init._hands == [dagger, shortsword]

    character_init.unequip()
    character_init.equip(longsword)

    assert character_init.equip(shield) == []
    assert character_init._hands == [longsword, shield]

    character_init.unequip()
    character_init.equip(greataxe)

    assert character_init.equip(dagger, True) == [greataxe]
    assert character_init._hands == [dagger]

    character_init.unequip()
    character_init.equip(greataxe)

    assert character_init.equip(shield, True) == [greataxe]
    assert character_init._hands == [shield]

    character_init.unequip()
    character_init.equip(dagger)
    character_init.equip(dagger)

    assert character_init.equip(shield, True) == [dagger]
    assert character_init._hands == [dagger, shield]

    assert character_init.equip(shield, True) == [shield]
    assert character_init._hands == [dagger, shield]

    assert character_init.equip(longsword, True) == [dagger]
    assert character_init._hands == [longsword, shield]

    assert character_init.equip(longsword, True) == [longsword]
    assert character_init._hands == [longsword, shield]

    assert character_init.equip(mace, True) == [longsword]
    assert character_init._hands == [mace, shield]

    character_init.unequip()
    character_init.equip(dagger)
    character_init.equip(shield)

    assert character_init.equip(dagger, True) == [shield]
    assert character_init._hands == [dagger, dagger]

    character_init.unequip()
    character_init.equip(dagger)
    character_init.equip(dagger)

    assert character_init.equip(dagger, True) == [dagger]
    assert character_init._hands == [dagger, dagger]

    assert character_init.equip(shortsword, True) == [dagger]
    assert character_init._hands == [shortsword, dagger]

    assert character_init.equip(shortsword, True) == [dagger]
    assert character_init._hands == [shortsword, shortsword]

    character_init.unequip()
    character_init.equip(dagger)
    character_init.equip(dagger)

    assert character_init.equip(greataxe, True, True) == [dagger, dagger]
    assert character_init._hands == [dagger, dagger]
    assert character_init.equip(longsword, True, True) == [dagger, dagger]
    assert character_init._hands == [dagger, dagger]
    assert character_init.equip(dagger, True, True) == [dagger]
    assert character_init._hands == [dagger, dagger]
    assert character_init.equip(shield, True, True) == [dagger]
    assert character_init._hands == [dagger, dagger]

    character_init.unequip()
    character_init.equip(leather_armor)

    assert character_init._armor == leather_armor
    assert character_init.equip(leather_armor, True, True) == [leather_armor]
    assert character_init._armor == leather_armor


def test_attack(character_init: Character) -> None:

    for attack_summary in character_init.attack_options(3, character_init):
        attack = character_init.attack(character_init, attack_summary)
        if attack.attack_hit.value <= 0:
            assert attack.damage == 0
        else:
            assert attack.damage > 0

    character_init.add_experience(dnd.EXPERIENCE_REQUIREMENT[-1])
    for attack_summary in character_init.attack_options(3, character_init):
        attack = character_init.attack(character_init, attack_summary)
        if attack.attack_hit.value <= 0:
            assert attack.damage == 0
        else:
            assert attack.damage > 0


def test_death_saving_throws(character_init: Character) -> None:

    throws = character_init.death_saving_throws()
    if throws:
        assert character_init._hitpoints == 1


def test_add_experience(character_init: Character) -> None:

    level = character_init._level
    xp = character_init._xp

    character_init.add_experience(1)
    assert character_init._xp == xp + 1
    assert character_init._level == level

    character_init.add_experience(dnd.EXPERIENCE_REQUIREMENT[-1])
    assert character_init._level == len(dnd.EXPERIENCE_REQUIREMENT)


def test_short_rest(character_init: Character) -> None:

    character_init.damage(1)
    hitpoints = character_init._hitpoints
    hitdice = character_init._hit_dice
    rest = character_init.short_rest()
    assert rest > 0
    assert character_init._hit_dice == hitdice - 1
    assert character_init._hitpoints == min(character_init._max_hitpoints, hitpoints + rest)


def test_long_rest(character_init: Character) -> None:

    hitdice = character_init._hit_dice
    character_init.damage(1)
    character_init._used_today["test"] = 10
    rest = character_init.long_rest()
    assert rest.hitpoints > 0
    assert character_init._hitpoints == character_init._max_hitpoints
    assert character_init._hit_dice >= hitdice
    for key in character_init._used_today:
        assert character_init._used_today[key] == 0


def test_title(character_init: Character) -> None:

    assert len(character_init.title()) > 0


def test_bio(character_init: Character) -> None:

    bio = character_init.bio()
    assert re.search(r"Str \d+ Dex \d+ Con \d+ Int \d+ Wis \d+ Cha \d+", bio)


def test_short_bio(character_init: Character) -> None:

    assert len(character_init.short_bio()) > 0


def test_extended_bio(character_init: Character) -> None:

    assert len(character_init.extended_bio()) > 0


def test_characters() -> None:

    races = dnd.get_races()
    classes = dnd.get_classes()

    for r in races:
        for c in classes:
            character = Character("test", dnd.get_race(r["index"]), dnd.get_class(c["index"]))
            test_init(character)
            test_proficient_weapons(character)
            test_proficient_armor(character)
            test_armor_class(character)
            test_spend(character)
            test_earn(character)
            test_equip(character)
            test_attack(character)
            test_add_experience(character)
            test_short_rest(character)
            test_long_rest(character)
            test_death_saving_throws(character)
