import requests

from src import dnd

# Amount of times to perform random tasks
N = 10


def test_globals() -> None:

    request = requests.get(dnd.BASE_URL)
    assert request.status_code == 200

    assert len(dnd.EXPERIENCE_REQUIREMENT) == 20
    assert len(dnd.EXCHANGE_RATES) > 0
    assert len(dnd.CACHE_PATH) > 0


def test_attack_result() -> None:

    result = dnd.AttackResult([], 5, dnd.ActionOutcome.SUCCESS, "test")
    assert result.damage == 5
    assert result.attack_hit == dnd.ActionOutcome.SUCCESS
    assert result.name == "test"


def test_attack_spell() -> None:

    spell = dnd.get_spell("sacred-flame")
    attack = dnd.AttackSpell(spell, 0, 1, 1)
    assert attack._damage_dice.max() == 8
    attack = dnd.AttackSpell(spell, 0, 1, 4)
    assert attack._damage_dice.max() == 8

    attack = dnd.AttackSpell(spell, 0, 1, 5)
    assert attack._damage_dice.max() == 16
    attack = dnd.AttackSpell(spell, 0, 1, 6)
    assert attack._damage_dice.max() == 16

    spell = dnd.get_spell("thunderwave")
    attack = dnd.AttackSpell(spell, 0, 1, 1)
    assert attack._damage_dice.max() == 16
    attack = dnd.AttackSpell(spell, 0, 2, 1)
    assert attack._damage_dice.max() == 24
    attack = dnd.AttackSpell(spell, 0, 10, 1)
    assert attack._damage_dice.max() == 80

    spells = dnd.get_spells()
    for s in spells:
        spell = dnd.get_spell(s)
        if "damage" in spell:
            attack = dnd.AttackSpell(spell, 0, 1, 1)
            assert attack._damage_dice.max() != 1


def test_reset_cache() -> None:

    dnd.reset_cache()

    assert dnd.session is not None
    dnd.session.get(dnd.BASE_URL)
    assert len(dnd.session.cache.responses) > 0

    dnd.reset_cache()

    assert dnd.session is not None
    assert len(dnd.session.cache.responses) == 0


def test_query() -> None:

    response = dnd.query("/api")

    assert isinstance(response, dict)
    assert len(response) > 0


def test_experience_level() -> None:

    assert dnd.experience_level(0) == 1
    assert dnd.experience_level(299) == 1
    assert dnd.experience_level(300) == 2
    assert dnd.experience_level(355001) == 20


def test_proficiency_ability() -> None:

    ability = dnd.get_proficiency_ability("skill-athletics")
    assert isinstance(ability, dict)
    assert len(ability) > 0

    ability = dnd.get_proficiency_ability("alchemists-supplies")
    assert len(ability) == 0


def test_get_monsters() -> None:

    monsters = dnd.get_monsters([0, 0.125, 0.25, 0.5])
    assert isinstance(monsters, list)
    assert len(monsters) > 0

    for m in monsters:
        monster = dnd.get_monster(m["index"])
        assert monster["challenge_rating"] in [0, 0.125, 0.25, 0.5]

    monsters = dnd.get_monsters([1])
    assert isinstance(monsters, list)
    assert len(monsters) > 0

    for m in monsters:
        monster = dnd.get_monster(m["index"])
        assert monster["challenge_rating"] == 1

    monsters = dnd.get_monsters([2, 5, 12])
    assert isinstance(monsters, list)
    assert len(monsters) > 0

    for m in monsters:
        monster = dnd.get_monster(m["index"])
        assert monster["challenge_rating"] in [2, 5, 12]


def test_get_monster_of_rating() -> None:

    for _ in range(N):
        monster = dnd.get_monster_of_rating([0, 0.125, 0.25, 0.5])
        assert monster["challenge_rating"] in [0, 0.125, 0.25, 0.5]

        monster = dnd.get_monster_of_rating([1])
        assert monster["challenge_rating"] == 1

        monster = dnd.get_monster_of_rating([2, 3])
        assert monster["challenge_rating"] in [2, 3]


def test_get_monster_lower() -> None:

    for _ in range(N):
        monster = dnd.get_monster_lower()
        assert monster["challenge_rating"] <= 0.5

        monster = dnd.get_monster_lower(20)
        assert monster["challenge_rating"] <= 20

        monster = dnd.get_monster_lower(0)
        assert monster["challenge_rating"] == 0

        monster = dnd.get_monster_lower(0.3)
        assert monster["challenge_rating"] in [0, 0.125, 0.25]


def test_get_monster_image() -> None:

    monster = dnd.get_monster("aboleth")
    assert len(dnd.get_monster_image_link(monster)) > 0

    monster = dnd.get_monster("deer")
    assert len(dnd.get_monster_image_link(monster)) == 0


def test_xp_to_max_cr() -> None:

    for i in range(dnd.EXPERIENCE_REQUIREMENT[-1] + 1):
        assert 0.25 <= dnd.xp_to_max_cr(i) <= 30


def test_get_weapon_ability() -> None:

    weapon = dnd.get_equipment("club")
    abilities = dnd.get_weapon_ability(weapon)
    assert isinstance(abilities, list)
    assert len(abilities) == 1
    assert abilities == ["str"]

    weapon = dnd.get_equipment("blowgun")
    abilities = dnd.get_weapon_ability(weapon)
    assert isinstance(abilities, list)
    assert len(abilities) == 1
    assert abilities == ["dex"]

    weapon = dnd.get_equipment("dagger")
    abilities = dnd.get_weapon_ability(weapon)
    assert isinstance(abilities, list)
    assert len(abilities) == 2
    assert abilities == ["str", "dex"]


def test_is_aquatic_weapon() -> None:

    weapon = dnd.get_equipment("dagger")
    assert dnd.is_aquatic_weapon(weapon)

    weapon = dnd.get_equipment("crossbow-light")
    assert dnd.is_aquatic_weapon(weapon)

    weapon = dnd.get_equipment("club")
    assert not dnd.is_aquatic_weapon(weapon)


def test_exchange() -> None:

    assert dnd.exchange("cp", 1) == 1
    assert dnd.exchange("sp", 1) == 1
    assert dnd.exchange("ep", 1) == 1
    assert dnd.exchange("gp", 1) == 1
    assert dnd.exchange("pp", 1) == 10

    assert dnd.exchange("gp", 123, "cp") == 12300
    assert dnd.exchange("cp", 10, "gp", False) == 0.1


def test_ability_modifier() -> None:

    assert dnd.ability_modifier(10) == 0
    assert dnd.ability_modifier(8) == -1
    assert dnd.ability_modifier(12) == 1
    assert dnd.ability_modifier(0) == -5
    assert dnd.ability_modifier(20) == 5


def test_cr_proficiency() -> None:

    assert dnd.cr_proficiency(0) == 2
    assert dnd.cr_proficiency(0.125) == 2
    assert dnd.cr_proficiency(4) == 2
    assert dnd.cr_proficiency(5) == 3
    assert dnd.cr_proficiency(28) == 8
    assert dnd.cr_proficiency(29) == 9
    assert dnd.cr_proficiency(30) == 9
