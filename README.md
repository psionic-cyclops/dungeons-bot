<div align="center">
<a href="https://mastodon.social/@dungeons" rel="me">
<img src="images/banner-large.png" width="100%">
</a>
<br />
<a href="https://mastodon.social/@dungeons" rel="me">
<img src="https://img.shields.io/mastodon/follow/109354069933492315?style=social">
</a>

<h1>Dungeons Bot</h1>
</div>

[Mastodon](https://joinmastodon.org/) bot that simulates D&D-like adventures through public polls. Utilizes [Mastodon.py](https://github.com/halcy/Mastodon.py) and the [D&D 5e SRD API](https://dnd5eapi.co).

**Official account: <a href="https://mastodon.social/@dungeons" rel="me">@dungeons@mastodon.social</a>**

Test account: <a href="https://mastodon.social/@dungeonsbeta" rel="me">@dungeonsbeta@mastodon.social</a>

[**Help & Extra Information**](https://dungeons.astrelion.com)

## Features

*Unchecked features are features I'd like to implement, not a commitment.*

- [x] Random character creation with names chosen from followers
- [x] Simple combat between random monsters
- [ ] Advanced combat between monsters (spells, feats, attack types, etc.)
- [x] Leveling up
- [ ] Loot
- [ ] Dungeons (back-to-back monster fights, loot)
- [x] Neutral town areas for shopping, rest, etc.
- [ ] Maps (towns, terrains, dungeons, etc.)
- [ ] Structured stories/missions
- [ ] File/node-driven maps/stories for custom campaigns
- [ ] Homebrew content support
- [x] Saving (state is saved between runs of the app)
- [ ] Query campaign/character info by commenting or DMs
- [x] Static website for docs and information
- [ ] Dynamic website for full character stats, campaign history, etc.
- [ ] Split the D&D API interface code, D&D campaign manager, and D&D campaign + Mastodon into separate packages

## Running yourself

### Create Mastodon account & application

1. Create a [Mastodon account](https://joinmastodon.org/servers)
2. Create a new application on your Mastodon account (e.g. `https://<server>/settings/applications/new`)
    - You should also probably check 'This is a bot account' in your profile settings (e.g. `https://<server>/settings/profile`)
3. Copy the access token for the application

### Setup App

#### Docker

1. Create the [docker-compose file](docker-compose.yml) (or [build yourself](Dockerfile))
2. Copy files from `data/` to `/opt/dungeons/` or whatever directory you specify in the `volumes` section
    - Copy `.token.example` to `.token` and enter your token and Mastodon URL
3. Run
    ```shell
    docker-compose up -d
    ```

#### Traditional

1. [Clone the project](https://gitlab.com/ASTRELION/dungeons-bot.git)
2. Copy [`config/.token.example`](config/.token.example) to `data/.token` and add your token and Mastodon URL
3. Install prerequisites with `pip3 install -r requirements.txt`
4. Run with `python3 main.py`

*Note that according to the GNU AGPLv3 [License](LICENSE), you must provide the source code to the application and honor the original License.*

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

```
Dungeons Bot
Copyright (C) 2023  ASTRELION

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

## Donate

[Liberapay](https://liberapay.com/ASTRELION/)

<a href="https://liberapay.com/ASTRELION/"><img src="https://img.shields.io/liberapay/patrons/ASTRELION.svg?logo=liberapay"></a>
