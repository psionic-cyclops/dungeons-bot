import json
import logging
import os
import shutil
import toml


SECONDS_PER_DAY = 86400
SECONDS_PER_HOUR = 3600
SECONDS_PER_MINUTE = 60


def debug(text: str):
    """Log a debug message.

    `text`: text to log.
    """
    logger.debug(text)


def error(text: str):
    """Log an error message.

    `text`: text to log.
    """
    logger.error(text)


def warn(text: str):
    """Log a warning message.

    `text`: text to log.
    """
    logger.warning(text)


def info(text: str):
    """Log a info message.

    `text`: text to log.
    """
    logger.info(text)


def format_duration(seconds: int) -> str:
    """Formats the given time in seconds as Days, HH:MM

    `seconds`: amount of seconds to format.
    """
    seconds = int(seconds)
    days, seconds = divmod(seconds, SECONDS_PER_DAY)
    hours, seconds = divmod(seconds, SECONDS_PER_HOUR)
    minutes, seconds = divmod(seconds, SECONDS_PER_MINUTE)

    if days == 1:
        return f"{days} day, {hours:02d}:{minutes:02d}"

    if days > 0:
        return f"{days} days, {hours:02d}:{minutes:02d}"

    return f"{hours:02d}:{minutes:02d}"


def get_clock(seconds: int) -> str:
    """Get the clock emoji associated with the given amount of seconds.

    `seconds`: amount of seconds to get the emoji for.
    """
    seconds = int(seconds)
    _, seconds = divmod(seconds, SECONDS_PER_DAY)
    hours, seconds = divmod(seconds, SECONDS_PER_HOUR)
    minutes, seconds = divmod(seconds, SECONDS_PER_MINUTE)

    hours = hours % 12 or 12
    base = 0x1f550 # 🕐

    if minutes >= 30:
        base = 0x1f55c # 🕜

    return f"{chr(base + hours - 1)}"


def int_in_array(obj: list | int, num: int) -> bool:
    """Find if a given int is in an arbitrarily nested int array

    `obj`: list or int to search.
    `num`: number to search for.
    """
    if isinstance(obj, int) and obj == num:
        return True
    elif isinstance(obj, list):
        return any(int_in_array(element, num) for element in obj)
    return False


def sum_array(array: list) -> int:
    """Sum an arbitrarily nested int array

    `array`: sum every number in a nested array.
    """
    total = 0
    for item in array:
        if isinstance(item, int):
            total += item
        elif isinstance(item, list):
            total += sum_array(item)
    return total


def sort_multi(array: list) -> list:
    """Sort a multidimensional array. Returns a new array.
    """
    new_array = []
    for element in array:
        if isinstance(element, list):
            new_array.append(sort_multi(element))
        else:
            new_array.append(element)
    return sorted(new_array)


def float_to_frac(number: float) -> str:
    """Convert a decimal number to a fraction string.
    e.g. 0.25 -> "1/4", 4 -> "4"
    """
    if int(number) == number:
        return str(number)

    nums = number.as_integer_ratio()
    return f"{nums[0]}/{nums[1]}"


def key_or_less(key: str, dictionary: dict[str]) -> str:
    """Get the given key from the dictionary, or a key with a value less
    than `key`, if it exists.

    `key`: key to get, should be cast-able to int.
    `dictionary`: dict to search.
    """
    if key in dictionary:
        return key

    keys = list(dictionary)
    i = len(keys) - 1
    max_key = keys[i]
    while int(max_key) > int(key) and i > 0:
        i -= 1
        max_key = keys[i]

    return max_key


def try_load(file_name: str) -> dict:
    """Try to load a configuration file, creating the default if it cannot be loaded.

    `file_name`: file name to load, assumes default directory `config/` and data directory `data/`
    """
    default_data = {}
    # load defaults
    with open(os.path.join(root, f"config/{file_name}"), "r", encoding="UTF-8") as file:
        default_data = {}
        if file_name.endswith(".toml"):
            default_data = toml.load(file)
        elif file_name.endswith(".json"):
            default_data = json.load(file)

    # load existing
    try:
        with open(os.path.join(root, f"data/{file_name}"), "r", encoding="UTF-8") as file:
            existing_data = {}
            if file_name.endswith(".toml"):
                existing_data = toml.load(file)
            elif file_name.endswith(".json"):
                existing_data = json.load(file)

            def set_values(dict_a: dict, dict_b: dict):
                for key in dict_a:
                    if key in dict_b:
                        if isinstance(dict_a[key], dict):
                            set_values(dict_a[key], dict_b[key])
                        elif not key.startswith("_"):
                            dict_a[key] = dict_b[key]

            set_values(default_data, existing_data)

    # or, create defaults
    except FileNotFoundError:
        pass

    with open(os.path.join(root, f"data/{file_name}"), "w", encoding="UTF-8") as file:
        if file_name.endswith(".json"):
            json.dump(default_data, file)
        elif file_name.endswith(".toml"):
            toml.dump(default_data, file)

    return default_data


def save(file_name: str, data: dict) -> None:
    """Save the given data at the file name.

    `file_name`: name of the file, including file extension.
    `data`: dictionary of data to save.
    """
    with open(os.path.join(root, f"data/{file_name}"), "w", encoding="UTF-8") as file:
        if file_name.endswith(".json"):
            json.dump(data, file)
        elif file_name.endswith(".toml"):
            toml.dump(data, file)


def save_configs() -> None:
    """Save all config files to disk.
    """
    save("emojis.toml", emojis)
    save("config.toml", config)
    save("tips.toml", tips)
    save("blacklist.toml", blacklist)


logger = logging.getLogger("dungeons")
logger.setLevel(logging.INFO)
logging.basicConfig(format="%(asctime)s %(levelname)-8s %(message)s")

root = os.path.dirname(os.path.dirname(__file__))

# Ensure data dir exists
if not os.path.exists(os.path.join(root, "data/")):
    os.makedirs(os.path.join(root, "data/"))

# Init data
emojis = try_load("emojis.toml")
config = try_load("config.toml")
tips = try_load("tips.toml")
blacklist = try_load("blacklist.toml")

shutil.copyfile(os.path.join(root, "config/.token.example"), os.path.join(root, "data/.token.example"))
