from datetime import datetime, timezone
from mastodon import Mastodon, StreamListener, MastodonError, MastodonAPIError
import os
import random
import string
import sys
from time import sleep, monotonic
import multiprocessing

import util

# Maximum number of options in a poll
MAX_POLL_SIZE = 4
# Maximum length of a status
STATUS_LIMIT = 500
# Maximum length of a single poll option
POLL_OPTION_LIMIT = 50
# Maximum number of times to retry an action before giving up
MAX_RETRIES = 6
# Seconds to wait before tries
RETRY_DELAY = 5
# Hashtag to use for tips
HASHTAG_TIP = "#tip"
# Content Warning for tips
CW_TIP = "Gameplay Tip"

# Whether to actually make posts or not
dry = False
# When dry, used for simulating poll ids
poll_id = 0
# When dry, used for keeping track of past poll options
last_poll: list[str] = []
# The last status ID that was posted
status_chain: int = None
# Mastodon client
mastodon: Mastodon = None
# Command listener
listener: "CommandListener" = None
# Stream handler for commands
command_handler = None
# Thread to post tips on
tip_thread: multiprocessing.Process = None


def init_client():
    """Initialize the Mastodon connection.
    """
    global mastodon
    global listener
    global command_handler
    mastodon = Mastodon(access_token=os.path.join(util.root, "data/.token"))
    listener = CommandListener()
    command_handler = mastodon.stream_user(listener, run_async=True, reconnect_async=True, reconnect_async_wait_sec=10)


def close():
    """Close connections.
    """
    global tip_thread
    global command_handler
    if not command_handler.closed:
        command_handler.close()
    if tip_thread.is_alive():
        tip_thread.kill()


def start_tips():
    """Start a thread to display tips on an interval.
    """
    global tip_thread
    tip_thread = multiprocessing.Process(target=tips, daemon=True)
    tip_thread.start()


def tips():
    """Posts tips on an interval.
    """
    mastodon = Mastodon(access_token=os.path.join(util.root, "data/.token"))
    interval = util.config["tip_interval"]
    next_time = monotonic() + interval

    while mastodon:
        sleep(max(0, next_time - monotonic()))
        util.info("Posting tip")
        tip = random.choice(list(util.tips.values()))
        tip += f"\n{HASHTAG_TIP}"
        if not dry:
            mastodon.status_post(tip, spoiler_text=CW_TIP)
        # Skips execution if posting took longer than interval
        next_time += ((monotonic() - next_time) // (interval * interval)) + interval


def command_reply(status_text: str, reply_to: dict) -> dict:
    """Reply to a user that issued a command.

    `status_text`: the content of the reply.
    `reply_to`: the status to reply to.
    """
    if dry:
        global poll_id
        poll_id += 1
        return {
            "id": poll_id
        }

    util.debug("Replying to command...")
    status_text = f"\n{status_text}"
    if len(status_text) > STATUS_LIMIT:
        status_text = status_text[:STATUS_LIMIT - 3] + "..."

    mastodon = Mastodon(access_token=os.path.join(util.root, "data/.token"))
    status = {}
    tries = 0

    # Try to respond to command
    while not status and tries < MAX_RETRIES:
        try:
            mastodon.status(reply_to["id"])
            status = mastodon.status_reply(reply_to, status_text, untag=True)
            util.debug(f"Replied to command")
        except MastodonError:
            tries += 1
            error_type, _, _ = sys.exc_info()
            util.warn(f"Could not reply to {reply_to['id']} ({error_type}), trying again in 5s...")
            sleep(RETRY_DELAY)

    return status


def post_status(status_text: str, last_result: str = "", chain=True) -> dict:
    """Post a plain-text status, returning its data.

    `status_text`: text to populate the post with.
    `last_result`: result of the last poll, prepended to `status_text`.
    `chain`: whether to chain (reply to) the previous status.
    """
    util.info("Posting status")
    global status_chain

    if dry:
        global poll_id
        poll_id += 1
        status_chain = poll_id
        return {
            "id": poll_id
        }

    status_text = f"{last_result}\n---\n{status_text}" if last_result else status_text
    if len(status_text) > STATUS_LIMIT:
        status_text = status_text[:STATUS_LIMIT - 3] + "..."

    status = mastodon.status_post(status_text, in_reply_to_id=status_chain if chain else None)
    status_chain = status["id"]
    return status


def post_poll(status_text: str, last_result: str, options: list[str], chain=True, extend=False) -> dict:
    """Post a poll, returning its data.

    `status_text`: text to populate the poll body with.
    `last_result`: result of the last poll, prepended to `status_text`.
    `chain`: whether to chain (reply to) the previous status.
    `extend`: whether to extend the poll time beyond normal.
    """
    util.info("Posting poll")
    global status_chain

    if dry:
        global last_poll
        global poll_id
        last_poll = options
        poll_id += 1
        status_chain = poll_id
        return {
            "id": poll_id,
            "poll": {}
        }

    duration = util.config["default_poll_duration"]
    if extend:
        duration *= util.config["no_vote_scale"]
    poll = mastodon.make_poll(options, duration)

    status_text = f"{last_result}\n---\n{status_text}" if last_result else status_text
    if len(status_text) > STATUS_LIMIT:
        status_text = status_text[:STATUS_LIMIT - 3] + "..."

    for i in range(len(options)):
        if len(options[i]) > POLL_OPTION_LIMIT:
            options[i] = options[i][:POLL_OPTION_LIMIT - 3] + "..."

    status = mastodon.status_post(status_text, poll=poll, in_reply_to_id=status_chain if chain else None)
    status_chain = status["id"]
    return status


def delete_post(status_id: int) -> dict:
    """Delete the given post, returning its data.

    `status_id`: ID of the status to delete.
    """
    global status_chain

    if dry:
        if status_chain == status_id:
            status_chain = poll_id - 1

        return {
            "id": poll_id
        }

    status = mastodon.status_delete(status_id)
    if status["id"] == status_chain:
        status_chain = status["in_reply_to_id"]

    return status


def poll_result(status_id: int, chain=True, option_groups: list = None) -> int:
    """Get poll results from the post with given ID.
    This is a blocking call and will wait until the poll is expired.

    `status_id`: ID of the status to query.
    `chain`: whether to chain (reply to) the previous status (applicable only if the poll is remade).
    `option_groups`: list of poll indexes to group and count together before individually.
    """
    util.debug("Waiting for poll to end...")
    global status_chain

    if dry:
        return random.randrange(0, len(last_poll))

    sleep(max(1, util.config["default_poll_duration"] - util.config["poll_check_time"]))
    status = mastodon.status(status_id)

    if "poll" not in status:
        raise ValueError("Status does not contain a poll.")

    diff = status["poll"]["expires_at"] - datetime.now(timezone.utc)
    sleep(max(1, diff.total_seconds() - util.config["poll_check_time"]))

    # update status info
    status = mastodon.status(status_id)
    poll = status["poll"]

    util.debug("Probing poll...")

    # probe poll
    if not poll["expired"]:
        diff = poll["expires_at"] - datetime.now(timezone.utc)
        # Poll has votes, so just wait for completion
        if poll["votes_count"] > 0:
            sleep(max(1, diff.total_seconds() + 1))
        # No votes yet, extend poll time
        else:
            old_status = delete_post(status_id)
            new_status = post_poll(old_status["text"], "", [p["title"] for p in poll["options"]], chain, True)
            util.debug("Poll has no votes, reposting...")
            return poll_result(new_status["id"], chain)

    # gather results
    util.debug("Polling results...")

    if option_groups is None:
        option_groups = []
    votes = [result["votes_count"] for result in poll["options"]]

    # Add any index not in a group as its own group
    for i in range(len(votes)):
        if not util.int_in_array(option_groups, i):
            option_groups.append([i])

    return find_winning_index(votes, util.sort_multi(option_groups))


def find_winning_index(votes: list[int], groups: list = None):
    """Find the winning index of an array of votes, given an arbitrarily nested
    int array of groups. For example, `[[0, 1], [2, 3]]` will group votes with
    index `0`, `1` together and groups with index `2`, `3` together. Groups are
    compared together by sum first, then the winning index within that group
    is returned. Assumes all indexes are somewhere in a group exactly once.

    `votes`: list of vote counts.
    `groups`: list of vote indexes to group together.
    """
    # Base case 1, no groups
    if not groups:
        return votes.index(max(votes))

    # Base case 2, array of ints (simple group)
    if all(isinstance(group, int) for group in groups):
        max_i = groups[0]
        biggest = votes[groups[0]]
        for i in groups:
            if votes[i] > biggest:
                biggest = votes[i]
                max_i = i
        return max_i

    group_sums = [] # sum of votes for the group
    indexes = [] # winning indexes of each group
    for group in groups:
        group_sums.append(util.sum_array([votes[i] for i in group]))
        indexes.append(find_winning_index(votes, group))

    return indexes[group_sums.index(max(group_sums))]


def get_followers() -> set[str]:
    """Get the list of follower names.
    """
    if dry:
        followers = set()
        for _ in range(100):
            followers.add("".join(random.choices(string.printable, k=random.randint(1, 16))))
        return followers

    followers = mastodon.fetch_remaining(mastodon.account_followers(mastodon.me()))
    return {f["display_name"] for f in followers if str(f["id"]) not in util.blacklist["b"]}


def update_profile(bio: str) -> dict:
    """Update the user profile based upon the Character description.

    `bio`: the string to update the bot bio with.
    """
    if dry:
        return {
            "note": "This is a bio"
        }

    return mastodon.account_update_credentials(note=bio)

class CommandListener(StreamListener):

    def __init__(self) -> None:

        self._command_handlers: list[function] = []
        super().__init__()


    # {
    #     'id': # id of the notification
    #     'type': # "mention", "reblog", "favourite", "follow", "poll" or "follow_request"
    #     'created_at': # The time the notification was created
    #     'account': # User dict of the user from whom the notification originates
    #     'status': # In case of "mention", the mentioning status
    #             # In case of reblog / favourite, the reblogged / favourited status
    # }
    def on_notification(self, notification: dict) -> None:

        if notification["type"] == "mention":
            util.debug(f"Received mention")
            for handler in self._command_handlers:
                handler(notification["account"], notification["status"])

        return super().on_notification(notification)


    # {
    #     'id': # The ID of this conversation object
    #     'unread': # Boolean indicating whether this conversation has yet to be
    #             # read by the user
    #     'accounts': # List of accounts (other than the logged-in account) that
    #                 # are part of this conversation
    #     'last_status': # The newest status in this conversation
    # }
    def on_conversation(self, conversation: dict) -> None:

        util.debug("Received DM")
        for handler in self._command_handlers:
            handler(conversation["accounts"][0], conversation["last_status"])

        return super().on_conversation(conversation)


    def add_handler(self, callback) -> int:
        """Add a function to be called on mention or DM.
        """
        util.debug("Added command handler")
        self._command_handlers.append(callback)
        return len(self._command_handlers)
