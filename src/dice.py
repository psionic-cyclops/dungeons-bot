import random
import re

import util


class _DiceElement:
    """Represents a single element in a Dice string.
    """

    def __init__(self, element: str) -> None:
        """Create a Dice element from string.

        `element`: a Dice element, e.g. "+4", "+1d4", "+MOD"
        """
        self._sign = 1
        self._die_count = 0
        self._face_count = 0
        self._element = element
        self._is_mod = False
        self._parse(element)


    def _parse(self, element: str) -> None:
        """Parse the element into this `_DiceElement`.

        `element`: Dice element string to parse
        """
        match = re.match(r"([+-]?)(\d+d\d+|\d+|MOD)", element)
        dice = match.group(2)
        if dice == "MOD":
            self._is_mod = True
        else:
            if dice.isdigit():
                dice = f"{dice}d1"
            self._die_count = int(re.match(r"(\d+)d\d+", dice).group(1))
            self._face_count = int(re.match(r"\d+d(\d+)", dice).group(1))

        self._sign = -1 if match.group(1) == "-" else 1


    def roll(self, custom_mod = 0) -> list[int]:
        """Roll this element.

        `custom_mod`: if this element is a MOD, replaces it with `custom_mod`
        """
        if self._is_mod:
            return [custom_mod * self._sign]
        return [random.randint(1, self._face_count) * self._sign for _ in range(self._die_count)]


    def __str__(self) -> str:
        """String representation of `_DiceElement`.
        """
        return self._element


class Dice:
    """Simple dice wrapper.
    """

    def __init__(self, dice: str) -> None:
        """Create a Dice from a dice string (ex. "2d20+2"), or a constant (ex. "4").
        """
        self._dice = dice.replace(" ", "")
        self._elements: list[_DiceElement] = []
        self._parse(self._dice)


    @staticmethod
    def is_dice(dice: str) -> bool:
        """Returns True if the given string is a dice roll.

        This allows basic dice forms like '1d4', '1d4+2', etc.
        'MOD' is also a valid modifier, e.g. '1d4+MOD'.

        `dice`: dice string to check.
        """
        dice = dice.replace(" ", "")
        if dice.isdigit():
            return True
        return bool(re.match(r"^([+-]?(\d+d\d+|\d+|MOD))+$", dice))


    def _parse(self, dice: str) -> None:
        """Parse the given dice string into this Dice object.

        `dice`: dice string to parse into this `Dice`.
        """
        # Change constants to dice form, e.g. "4" -> "4d1"
        if dice.isdigit():
            dice = f"{dice}d1"

        if not self.is_dice(dice):
            raise ValueError(f"Invalid dice format {dice}")

        elements = re.findall(r"([+-]?(\d+d\d+|\d+|MOD))", dice)
        for element in elements:
            self._elements.append(_DiceElement(element[0]))


    def roll(self, count = 1, highest = 0, lowest = 0, custom_mod = 0) -> int:
        """Roll the given amount of dice and return the sum.

        `count`: number of times to roll
        `highest`: if `count > 1`, returns the top `highest` rolls
        `lowest`: if `count > 1`, returns the bottom `lowest` rolls
        `custom_mod`: custom roll-time modifier, replaces all instances of 'MOD' in Dice string

        If both `highest` and `lowest` are provided, the result will be a non-overlapping
        sum of the top `highest` rolls and bottom `lowest` rolls.
        """
        rolls = []
        for _ in range(count):
            roll = 0
            for element in self._elements:
                roll += sum(element.roll(custom_mod))
            util.debug(f"Rolled {self} -> {roll}")
            rolls.append(roll)

        if count > 1 and (highest > 0 or lowest > 0):
            rolls_sum = 0

            if highest > 0:
                rolls.sort(reverse=True)
                highs = rolls[:highest]
                [rolls.remove(h) for h in highs]
                rolls_sum += sum(highs)

            if lowest > 0:
                rolls.sort()
                lows = rolls[:lowest]
                [rolls.remove(l) for l in lows]
                rolls_sum += sum(lows)

            return rolls_sum

        return sum(rolls)


    def roll_advantage(self, count = 1) -> int:
        """Roll with advantage; alias for `roll(2, highest=1)`.

        `count`: amount of advantage, 2 for double advantage, 3 for triple advantage, etc.
        """
        return self.roll(count + 1, highest=1)


    def roll_disadvantage(self, count = 1) -> int:
        """Roll with disadvantage; alias for `roll(2, lowest=1)`.

        `count`: amount of advantage, 2 for double disadvantage, 3 for triple disadvantage, etc.
        """
        return self.roll(count + 1, lowest=1)


    def max(self) -> int:
        """Get the maximum possible roll for this Dice.
        """
        m = 0
        for element in self._elements:
            m += element._die_count * element._face_count * element._sign
        return m


    def min(self) -> int:
        """Get the minimum possible roll for this Dice.
        """
        m = 0
        for element in self._elements:
            m += element._die_count * element._sign
        return m


    def mean(self) -> float:
        """Get the average roll for this Dice.
        """
        return self.min() + ((self.max() - self.min()) / 2)


    def __str__(self) -> str:
        """Get the string representation of the Dice.
        """
        return self._dice
