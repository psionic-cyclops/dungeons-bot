from enum import Enum
import math
import os
import random
import requests_cache

from dice import Dice
import util

# URL where the API is available.
# All requests will be prefixed with /api/
BASE_URL = "https://www.dnd5eapi.co"
# Experience requirement to reach each level.
# index 0 = level 1, index 1 = level 2, etc.
EXPERIENCE_REQUIREMENT = [
    0, 300, 900, 2700,               # 1-4   (prof 2)
    6500, 14000, 23000, 34000,       # 5-8   (prof 3)
    48000, 64000, 85000, 100000,     # 9-12  (prof 4)
    120000, 140000, 165000, 195000,  # 13-16 (prof 5)
    225000, 265000, 305000, 355000   # 17-20 (prof 6)
]
# Currency units valued by copper pieces
EXCHANGE_RATES = {
    "cp": 1,
    "sp": 10,
    "ep": 50,
    "gp": 100,
    "pp": 1000
}
# Weapons that do not have disadvantage when used underwater
AQUATIC_WEAPON = [
    "dagger",
    "javelin",
    "shortsword",
    "spear",
    "trident",
    "crossbow",
    "net",
    "dart",
]
# Path to cache requests (with no file extension)
CACHE_PATH = "data/cache"

session: requests_cache.CachedSession = None


class ActionOutcome(Enum):
    """Outcome of an action.

    `>0`: (crit) success
    `=0`: no outcome
    `<0`: (crit) failure
    """
    CRIT_FAILURE = -2
    FAILURE = -1
    NONE = 0
    SUCCESS = 1
    CRIT_SUCCESS = 2


class AttackRange(Enum):
    """Range of an attack, either Melee, Ranged, or Mixed.
    """
    MELEE = 0
    RANGED = 1
    MIXED = 2
    SPELL = 3

    @staticmethod
    def combine(range_1: "AttackRange", range_2: "AttackRange") -> "AttackRange":
        """Find the combination of two ranges.

        `range_1`: first `AttackRange`.
        `range_2`: second `AttackRange`.

        melee + ranged = mixed,
        melee + melee = melee,
        ranged + ranged = ranged
        """
        if range_1 == range_2:
            return range_1

        return AttackRange.MIXED


class Attack:

    def __init__(self, damage_dice: Dice, damage_type: str, range: AttackRange, modifier: int, proficient: bool) -> None:
        """Create an arbitrary Attack.

        `damage_dice`: damage Dice to roll for the attack.
        `damage_type`: damage type of the attack.
        """
        self._damage_dice = damage_dice
        self._damage_type = damage_type
        self._range = range
        self._proficient = proficient
        self._modifier = modifier


class AttackSpell(Attack):

    def __init__(self, spell: dict, modifier: int, slot_level: int = 1, caster_level: int = 1) -> None:
        """Create an attack with a Spell.

        `spell`: raw spell dict.
        `slot_level`: slot level used for the spell.
        `caster_level`: level of the Creature that cast the spell.
        """
        damage_dice = Dice("1")
        if "damage_at_character_level" in spell["damage"]:
            level = util.key_or_less(str(caster_level), spell["damage"]["damage_at_character_level"])
            damage_dice = Dice(spell["damage"]["damage_at_character_level"][level])
        if "damage_at_slot_level" in spell["damage"]:
            level = util.key_or_less(str(slot_level), spell["damage"]["damage_at_slot_level"])
            damage_dice = Dice(spell["damage"]["damage_at_slot_level"][level])

        self._is_attack = False
        spell_range = None

        if "attack_type" in spell:
            self._is_attack = True
            spell_range = AttackRange.RANGED
            if spell["attack_type"] == "melee":
                spell_range = AttackRange.MELEE
        elif spell["range"].lower() == "touch":
            spell_range = AttackRange.MELEE
        elif spell["range"].lower() == "self":
            spell_range = AttackRange.MELEE
            if "area_of_effect" in spell and spell["area_of_effect"]["size"] > 15:
                spell_range = AttackRange.RANGED
        else:
            feet = int(spell["range"].split(" ")[0])
            spell_range = AttackRange.MELEE if feet <= 15 else AttackRange.RANGED

        super().__init__(damage_dice, spell["damage"]["damage_type"]["index"], spell_range, modifier, True)

        self._spell = spell
        self._caster_level = caster_level
        self._slot_level = slot_level
        self._dc = {}
        if "dc" in spell:
            self._dc = spell["dc"]


    def __str__(self) -> str:
        """String representation of the `AttackSpell`
        """
        return ", ".join([
            self._spell["index"],
            str(self._slot_level),
            str(self._caster_level)
        ])


class AttackWeapon(Attack):

    def __init__(self, weapon: dict, damage_dice: Dice, damage_type: str, modifier: int, proficient: bool, range: AttackRange) -> None:
        """Create an attack with the described weapon.

        `weapon`: raw weapon dict
        `damage_dice`: damage dice to roll for this attack
        `damage_type`: damage type for this attack
        `modifier`: ability modifier for this account (attack and damage roll bonus)
        `proficient`: whether this attack should get proficiency bonus (attack roll bonus)
        `range`: range of this attack
        """
        super().__init__(damage_dice, damage_type, range, modifier, proficient)
        self._weapon = weapon


    def __str__(self) -> str:
        """String representation of the `AttackWeapon`.
        """
        return ", ".join([
            self._weapon["index"],
            str(self._damage_dice),
            self._damage_type,
            str(self._modifier),
            str(self._proficient),
            self._range.name
        ])


class AttackSummary:
    """
    Represents an attack and data to perform it.
    """

    def __init__(self, attacks: list[Attack] = None, prompt = "") -> None:
        """Create a new attack.

        `attacks`: list of `AttackWeapons` in this attack.
        `prompt`: prompt to display on polls for this attack.
        """
        self._attacks: list[Attack] = []
        if attacks is not None:
            for attack in attacks:
                self.add_attack(attack)
        self._prompt = prompt


    def add_attack(self, attack: Attack) -> None:
        """Add an attack.

        `attack_weapon`: `AttackWeapon` to add to this attack.
        """
        self._attacks.append(attack)


    def get_attacks(self) -> list[Attack]:
        """Get attacks.
        """
        return self._attacks


    def set_prompt(self, prompt: str) -> None:
        """Set the prompt for this attack.

        `prompt`: prompt string to set.
        """
        self._prompt = prompt


    def get_prompt(self) -> str:
        """Get the prompt for this attack.
        """
        return self._prompt


    def get_range(self) -> AttackRange:
        """Get the combined range of all attacks.
        """
        attack_range = self._attacks[0]._range
        for attack in self._attacks:
            attack_range = AttackRange.combine(attack_range, attack._range)
        return attack_range


    def __str__(self) -> str:
        """Get string representation of the AttackSummary.
        """
        return self.get_prompt()


    @staticmethod
    def combine(attack_1: "AttackSummary", attack_2: "AttackSummary") -> "AttackSummary":
        """Combine this attack with another, returning a new `AttackSummary` object.

        `attack_1`: the first `AttackSummary`.
        `attack_2`: the second `AttackSummary`.
        """
        attack_summary = AttackSummary()

        for attack in attack_1.get_attacks():
            attack_summary.add_attack(attack)
        for attack in attack_2.get_attacks():
            attack_summary.add_attack(attack)

        return attack_summary


class AttackResult:
    """Represents an attack result, with a damage amount, crit, and name.
    """

    def __init__(self, attacks: list[Attack], damage: int, attack_hit: ActionOutcome, name: str) -> None:
        """Create a new `AttackResult`.

        `attacks`: attacks used, only including attacks that hit.
        `damage`: damage of the attack.
        `attack_hit`: `ActionOutcome` of the attack.
        `name`: name of the attack.
        """
        self.attacks = attacks
        self.damage = damage
        self.attack_hit = attack_hit
        self.name = name


def reset_cache() -> None:
    """Reset the session cache for API requests.
    """
    global session
    if session:
        session.cache.clear()
        session.close()
        session = None

    session = requests_cache.CachedSession(os.path.join(util.root, CACHE_PATH))


def query(path: str, params: dict = None) -> dict:
    """Query the D&D API.

    `path`: API path to query, usually starts with '/api/'.
    `params`: URL parameters to pass to the request.
    """
    if not session:
        reset_cache()

    response = session.get(f"{BASE_URL}{path}", params=params)
    if response.from_cache:
        util.debug(f"Retrieved cache for {response.url}")
    else:
        util.debug(f"Requesting {response.url}")

    if response.status_code == 200:
        response_dict = response.json()
        return response_dict

    util.warn(f"Requesting {response.url} resulted in an invalid request.")
    return {}


def experience_level(experience_points: int) -> int:
    """Determine the corresponding level from the given experience points.

    `experience_points`: number of xp to determine the level for.
    """
    level = 0
    for i, exp_requirement in enumerate(EXPERIENCE_REQUIREMENT):
        if experience_points >= exp_requirement:
            level = i
        else:
            break

    return level + 1


def get_races() -> list[dict]:
    """Get a list of all available races.
    """
    return query("/api/races")["results"]


def get_race(race_index: str) -> dict:
    """Get information about a race.

    `race_index`: index of the race to get.
    """
    return query(f"/api/races/{race_index}")


def get_classes() -> list[dict]:
    """Get a list of all available classes.
    """
    return query("/api/classes")["results"]


def get_class(class_index: str) -> dict:
    """Get information about a class.

    `class_index`: index of the class to get.
    """
    return query(f"/api/classes/{class_index}")


def get_class_level(class_index: str, level: int) -> dict:
    """Get level information for a class and level.

    `class_index`: index of the class to get levels for.
    `level`: level to get data for.
    """
    return query(f"/api/classes/{class_index}/levels/{level}")


def get_abilities() -> list[dict]:
    """Get a list of all available ability scores.
    """
    return query("/api/ability-scores")["results"]


def get_ability(ability_index: str) -> dict:
    """Get information about an ability score.

    `ability_index`: index of the ability to get.
    """
    return query(f"/api/ability-scores/{ability_index}")


def get_proficiency(proficiency_index: str) -> dict:
    """Get information about a proficiency.

    `proficiency_index`: index of the proficiency to get.
    """
    return query(f"/api/proficiencies/{proficiency_index}")


def get_skills() -> list[dict]:
    """Get a list of all available skills.
    """
    return query("/api/skills")["results"]


def get_skill(skill_index: str) -> dict:
    """Get information about a skill.

    `skill_index`: index of the skill to get.
    """
    return query(f"/api/skills/{skill_index}")


def get_proficiency_ability(proficiency_index: str) -> dict:
    """Gets the ability related to the given proficiency.

    `proficiency_index`: index of the proficiency to get the ability for.
    """
    proficiency = get_proficiency(proficiency_index)["reference"]["index"]
    skill = get_skill(proficiency)
    if skill:
        return get_ability(skill["ability_score"]["index"])

    util.warn(f"Proficiency {proficiency_index} isn't associated with an ability.")
    return {}


def get_features() -> list[dict]:
    """Get a list of all available features.
    """
    return query("/api/features")["results"]


def get_feature(feature_index: str) -> dict:
    """Get information about a feature.

    `feature_index`: index of the feature to get.
    """
    return query(f"/api/features/{feature_index}")


def get_spells() -> list[dict]:
    """Get a list of all available spells.
    """
    return query("/api/spells")["results"]


def get_spell(spell_index: str) -> dict:
    """Get information about a spell.
    """
    return query(f"/api/spells/{spell_index}")


def get_class_spells(class_index: str, spell_level: int) -> list[dict]:
    """Get a list of all available spells for the given class and level.
    """
    # if spell_level == 2:
    #     return []
    return query(f"/api/classes/{class_index}/levels/{spell_level}/spells")["results"]


def get_equipment(equipment_index: str) -> dict:
    """Get information about an equipment.

    `equipment_index`: index of the equipment to get.
    """
    return query(f"/api/equipment/{equipment_index}")


def get_equipment_category(category_index: str) -> dict:
    """Get information about an equipment category.

    `category_index`: index of the equipment category to get.
    """
    return query(f"/api/equipment-categories/{category_index}")


def get_monster(monster_index: str) -> dict:
    """Get information about a monster.

    `monster_index`: index of the monster to get.
    """
    return query(f"/api/monsters/{monster_index}")


def get_monsters(challenge_ratings: list[float] = None) -> list[dict]:
    """Get a list of monsters based on challenge rating.

    `challenge_ratings`: challenge ratings to filter by.
    """
    return query("/api/monsters", {"challenge_rating": challenge_ratings})["results"]


def get_monster_of_rating(challenge_ratings: list[float] = None) -> dict:
    """Get a random monster of the given ratings. A random rating is chosen first, then
    a monster from the resulting rating.

    `challenge_ratings`: challenge ratings to filter by.
    """
    ratings = challenge_ratings.copy()
    monsters = None
    while not monsters and ratings:
        monster_cr = random.choice(ratings)
        ratings.remove(monster_cr)
        monsters = get_monsters([monster_cr])
    monster_index = random.choice(monsters)["index"]
    return get_monster(monster_index)


def get_monster_lower(challenge_rating=0.5) -> dict:
    """Get a monster with the given `challenge_rating` or lower

    `challenge_rating`: challenge rating to filter by, includes every CR below.
    """
    challenge_ratings = [0, 0.125, 0.25, 0.5][:math.floor(challenge_rating * 8) + 1]
    challenge_ratings += [i + 1 for i in range(math.floor(challenge_rating))]
    return get_monster_of_rating(challenge_ratings)


def get_monster_image_link(monster: dict) -> str:
    """Get the image link associated with a monster, if any.

    `monster`: monster dict to get the image link for.
    """
    link = ""
    if "image" in monster:
        link = f"{BASE_URL}{monster['image']}"

    return link.replace("www.", "")


def xp_to_max_cr(experience_points: int) -> float:
    """Converts the given Character xp to maximum Monster Challenge Rating.

    This allows monsters to gradually increase in difficulty as the Character
    gains xp, not just on level.

    `experience_points`: Character xp to convert.
    """
    cr = math.floor((30 / math.sqrt(355000)) * math.sqrt(experience_points))
    if experience_points < 900:
        cr = [0, 0.125, 0.25, 0.5][math.floor((((0.5 / 1800) * experience_points) + 0.25) * 8)]
    return min(30, cr)


def get_weapon_ability(weapon: dict) -> list[str]:
    """Get the abilities associated with the given weapon.

    `weapon`: weapon dict to get the associated ability for.
    """
    if equipment_has_property(weapon, "finesse"):
        return ["str", "dex"]

    if weapon["weapon_range"].lower() == "melee":
        return ["str"]

    if weapon["weapon_range"].lower() == "ranged":
        return ["dex"]

    # If no matches above:
    raise ValueError(f"Weapon ability could not be determined! ({weapon})")


def is_aquatic_weapon(weapon: dict) -> bool:
    """Determine if the given weapon can be used in water without disadvantage.

    `weapon`: weapon dict to determine aquatic for.
    """
    return any(aquatic in weapon["index"] for aquatic in AQUATIC_WEAPON)


def equipment_has_property(equipment: dict, property_index: str) -> bool:
    """
    Determine if the given equipment has the given property. Usually only applies
    to weapons.

    `equipment`: equipment dict to find property for.
    `property_index`: index of the property to find.
    """
    return "properties" in equipment and any(p["index"] == property_index for p in equipment["properties"])


# There is only 1 shield defined in the SRD, if homebrew content is supported
# this check may need to be more advanced.
def equipment_is_shield(equipment: dict) -> bool:
    """Determine if the given equipment is a shield (armor that is held in a hand).

    `equipment`: equipment dict to determine shield.
    """
    return "armor_category" in equipment and equipment["armor_category"].lower() == "shield"


def exchange(unit_from: str, quantity: int, unit_to="gp", rounded=True) -> float:
    """Exchange currency from one currency unit to another.
    If `round` is True, the resulting conversion is rounded up. Ex. 1 sp -> gp = 1 gp

    `unit_from`: unit of currency to convert from.
    `quantity`: quantity of currency to convert.
    `unit_to`: unit of currency to convert to.
    `rounded`: whether to round the result up or not.
    """
    exchanged = (EXCHANGE_RATES[unit_from] * quantity) / EXCHANGE_RATES[unit_to]
    if rounded:
        exchanged = math.ceil(exchanged)

    return exchanged


def ability_modifier(ability_score: int) -> int:
    """Get the ability modifier for a given score.

    `ability_score`: ability score value to determine the modifier for.
    """
    return math.floor((ability_score - 10) / 2)


def cr_proficiency(cr: float) -> int:
    """Determine the proficiency bonus from the given challenge rating.

    `cr`: CR to determine proficiency bonus for.
    """
    return 2 if cr == 0 else math.ceil(cr / 4) + 1
