import copy
import math
import random
from collections import namedtuple

import client
from dice import Dice
from creature import Creature
import dnd
import lang
import util


class SpellSlot:

    def __init__(self, spell_slot_level: int, max_charges = 0, cantrip = False) -> None:
        """Create a new Spell Slot of the given level."""
        self._level = spell_slot_level
        self._max_charges = max_charges
        self._current_charges = self._max_charges
        self._cantrip = cantrip


    def recharge(self, count: int = None) -> int:
        """Replenish charges of this Spell Slot.

        `count`: restore the given amount of charges, otherwise restores all charges.
        """
        old_current = self._current_charges
        if count is None:
            self._current_charges = self._max_charges
        else:
            self._current_charges = min(self._max_charges, self._current_charges + count)
        return self._current_charges - old_current


    def can_use(self) -> bool:
        """Whether this Spell Slot level can be used.
        This determines if a Character can cast a spell at this level.
        """
        return self._max_charges > 0 or self._cantrip


    def has_charge(self) -> bool:
        """Whether this Spell Slot has at least 1 charge.
        """
        return self._current_charges > 0 or self._cantrip


    def use(self) -> int:
        """Use this slot and return the amount of charges.
        """
        self._current_charges = max(0, self._current_charges - 1)
        return self._current_charges


    def __str__(self) -> str:

        return f"{self._level}:{self._current_charges}/{self._max_charges}"


class Character(Creature):
    """
    Describes a player Character.
    """
    ORDERED_ABILITY_INDEXES = ("str", "dex", "con", "int", "wis", "cha")

    def __init__(self, name: str, race: dict = None, clazz: dict = None) -> None:
        """Create a new D&D character with a random race, class, and stats.

        `name`: name of the Character.
        `race`: race dict of the Character.
        `clazz`: class dict of the Character.
        """
        super().__init__(name.title())
        util.info(f"Creating character '{name}'")

        # Random race
        self._race = race
        if race is None:
            races = dnd.get_races()
            self._race = dnd.get_race(random.choice(races)["index"])

        # Random class
        self._class = clazz
        if clazz is None:
            classes = dnd.get_classes()
            self._class = dnd.get_class(random.choice(classes)["index"])

        # Level
        self._level = 1
        self._hit_dice = 1
        self._xp = 0

        self._features = []

        self._stats = {
            "kills": 0
        }


    def init(self) -> None:
        """Initialize the Character with data.
        """
        util.info("Initializing character")

        # Roll for ability scores
        rolls = []
        for _ in range(len(self._abilities)):
            rolls.append(Dice("1d6").roll(4, highest=3))

        rolls.sort()
        for ability in self.ability_priority():
            self._abilities[ability] = rolls.pop()

        # assign the rest randomly
        for ability in self._abilities:
            if self._abilities[ability] == 0:
                self._abilities[ability] = random.choice(rolls)
                rolls.remove(self._abilities[ability])

        # Apply racial ability bonuses
        for bonus in self._race["ability_bonuses"]:
            ability = bonus["ability_score"]["index"]
            self._abilities[ability] += bonus["bonus"]

        # Apply ability constraints
        for ability in self._abilities:
            self._abilities[ability] = min(20, self._abilities[ability])

        # Class proficiencies
        for p in self._class["proficiencies"]:
            self.add_proficiency(p["index"])

        # Race proficiencies
        for p in self._race["starting_proficiencies"]:
            self.add_proficiency(p["index"])

        # Choice (class) proficiencies
        prof_choice = self._class["proficiency_choices"][0]
        amount = prof_choice["choose"]
        choices = prof_choice["from"]["options"]
        for _ in range(amount):
            choice = random.choice(choices)
            choices.remove(choice)
            self.add_proficiency(choice["item"]["index"])

        weapon_options = []
        armor_options = []

        def add_equipment_option(equipment_index):
            equipment_item = dnd.get_equipment(equipment_index)
            if equipment_item["equipment_category"]["index"] == "weapon" and "damage" in equipment_item:
                weapon_options.append(equipment_item)
            elif equipment_item["equipment_category"]["index"] == "armor" and equipment_item["armor_category"].lower() != "shield":
                armor_options.append(equipment_item)
            else:
                util.debug("{} not a valid weapon or armor".format(equipment_index))

        def add_equipment_by_category(category):
            equipment = dnd.get_equipment_category(category)["equipment"]
            for e in equipment:
                add_equipment_option(e["index"])

        def add_equipment_for_options(options):
            for o in options:
                if o["option_type"] == "choice":
                    add_equipment_by_category(o["choice"]["from"]["equipment_category"]["index"])

                elif o["option_type"] == "multiple":
                    for t in o["items"]:
                        if t["option_type"] == "counted_reference":
                            add_equipment_option(t["of"]["index"])

                        elif t["option_type"] == "choice":
                            add_equipment_by_category(t["choice"]["from"]["equipment_category"]["index"])

                elif o["option_type"] == "counted_reference":
                    add_equipment_option(o["of"]["index"])

        # Add all base starting weapons
        for e in self._class["starting_equipment"]:
            add_equipment_option(e["equipment"]["index"])

        # TODO: make a class to manage equipement
        for option in self._class["starting_equipment_options"]:

            if (option["from"]["option_set_type"] == "equipment_category"):
                add_equipment_by_category(option["from"]["equipment_category"]["index"])

            elif (option["from"]["option_set_type"] == "options_array"):
                add_equipment_for_options(option["from"]["options"])

        self._hands = []
        self._armor = {}
        if (weapon_options):
            self.equip(random.choice(weapon_options))
        if (armor_options):
            self.equip(random.choice(armor_options))

        self._gp = Dice("3d4").roll() * 10

        # TODO: temporary, remove constant 12 when low hitpoints are more viable
        self.set_max_hitpoints(12 + self.ability_modifier("con"))
        self._hitpoints = self._max_hitpoints

        self._walk_speed = self._race["speed"]

        # Level (1) specific
        class_level = dnd.get_class_level(self._class["index"], self._level)

        self._class_specific = class_level["class_specific"]
        self._proficiency_bonus = class_level["prof_bonus"]

        for feature in class_level["features"]:
            self._features.append(dnd.get_feature(feature["index"]))

        self._spell_slots = [SpellSlot(l) for l in range(0, 10)]
        self._spells = [[] for _ in range(len(self._spell_slots))]
        self._spells_known = {
            "cantrips": 0,
            "spells": 0
        }
        self._spell_ability = ""

        if "spellcasting" in class_level:
            self._spell_ability = self._class["spellcasting"]["spellcasting_ability"]["index"]

            if "cantrips_known" in class_level["spellcasting"]:
                self._spell_slots[0] = SpellSlot(0, cantrip=True)

            for spellcast in class_level["spellcasting"]:
                # populate spell slots
                if spellcast.startswith("spell_slots_level_"):
                    level = int(spellcast[-1])
                    self._spell_slots[level] = SpellSlot(level, class_level["spellcasting"][spellcast])

            self.learn_spells()

        util.info(f"Created Character {self}")


    def unknown_spells(self) -> list[list]:
        """Get a list of unknown spells. Each index is a usable spell level.
        """
        spells = [[] for _ in range(len(self._spell_slots))]
        for slot in self._spell_slots:
            if not slot.can_use():
                continue

            all_spells = dnd.get_class_spells(self._class["index"], slot._level)
            for spell in all_spells:
                if not any(spell["index"] == s["index"] for s in self._spells[slot._level]):
                    s = dnd.get_spell(spell["index"])
                    spells[slot._level].append(s)

        return spells


    def learn_spells(self) -> tuple:
        """Learn spells if any are left to learn, and gain spell slots if applicable.

        Learning spells is random, but spells with damage have a higher chance of being
        learned.
        """
        class_level = dnd.get_class_level(self._class["index"], self._level)
        spells_learned = []
        slots_gained = [0] * len(self._spell_slots)

        if "spellcasting" not in class_level:
            return (spells_learned, slots_gained)
        spellcasting = class_level["spellcasting"]

        # update spell slots
        for slot in spellcasting:
            if slot.startswith("spell_slots_level_"):
                s = int(slot[-1])
                old_max = self._spell_slots[s]._max_charges
                self._spell_slots[s]._max_charges = spellcasting[slot]
                slots_gained[s] = spellcasting[slot] - old_max
                current = self._spell_slots[s]._current_charges
                self._spell_slots[s]._current_charges = min(current, spellcasting[slot])

        cantrip_count = spellcasting["cantrips_known"] if "cantrips_known" in spellcasting else 0
        spell_count = spellcasting["spells_known"] if "spells_known" in spellcasting else 0

        unknown_spells = self.unknown_spells()
        # give spells with damage higher probability
        weights = [[] for _ in range(len(unknown_spells))]
        for i, slot in enumerate(unknown_spells):
            weights[i] = [1] * len(slot)
            slot.sort(key=lambda s: "damage" in s, reverse=True)
            k = 0 # number of damaging spells
            for j, spell in enumerate(slot):
                if "damage" not in spell:
                    k = j
                    break
            weights[i][:k] = [2] * k

        # classes with special spell count features
        if self.has_feature("spellcasting-druid") or self.has_feature("spellcasting-cleric"):
            spell_count = max(1, self._level + self.ability_modifier("wis"))
        elif self.has_feature("spellcasting-paladin"):
            spell_count = max(1, math.floor(self._level / 2) + self.ability_modifier("cha"))
        elif self.has_feature("spellcasting-wizard"):
            spell_count = 6 + ((self._level - 1) * 2)

        # learn cantrips
        for _ in range(min(len(unknown_spells[0]), cantrip_count - self._spells_known["cantrips"])):
            if not unknown_spells[0]:
                continue
            cantrip = None
            while not cantrip and unknown_spells[0]:
                cantrip = random.choices(unknown_spells[0], weights=weights[0], k=1)[0]
                i = unknown_spells[0].index(cantrip)
                weights[0].pop(i)
                unknown_spells[0].pop(i)

            if not cantrip:
                continue
            self._spells[0].append(cantrip)
            spells_learned.append(cantrip)
            util.debug(f"Learned {cantrip['name']}")

        # learn spells
        levels = [s._level for s in self._spell_slots[1:] if s.can_use()]
        for _ in range(spell_count - self._spells_known["spells"]):
            level = None
            spell = None
            while not spell:
                levels = [l for l in levels if len(unknown_spells[l]) > 0]
                if not levels:
                    break
                level = random.choice(levels)
                spell = random.choices(unknown_spells[level], weights=weights[level], k=1)[0]
                i = unknown_spells[level].index(spell)
                weights[level].pop(i)
                unknown_spells[level].pop(i)

            if not spell:
                continue
            self._spells[level].append(spell)
            spells_learned.append(spell)
            util.debug(f"Learned {spell['name']}")

        self._spells_known["cantrips"] = cantrip_count
        self._spells_known["spells"] = spell_count
        return (spells_learned, slots_gained)


    def ability_priority(self) -> list[str]:
        """Get a list of abilities in order of best-assumed priority for this Character.

        Note: the purpose of this function is NOT to get the objectively "best" ability priority,
        but rather an ability priority that would be realistic for the Character while still
        introducing randomized elements for variety.
        """
        priority = set()

        # prefer primary ability scores, reflected in multi-classing requirements
        multi_classing = self._class["multi_classing"]
        if "prerequisites" in multi_classing:
            for prereq in multi_classing["prerequisites"]:
                priority.add(prereq["ability_score"]["index"])
        if "prerequisite_options" in multi_classing:
            options = [p["ability_score"]["index"] for p in multi_classing["prerequisite_options"]["from"]["options"]]
            random.shuffle(options)
            for prereq in options:
                priority.add(prereq)

        # then randomly-ordered saving throws
        saving_throws = [t["index"] for t in self._class["saving_throws"]]
        random.shuffle(saving_throws)
        for saving_throw in saving_throws:
            priority.add(saving_throw)

        # then con and dex and str
        priority.add("con") # for hitpoints
        dex_str = ["dex", "str"] # for armor and weapons
        random.shuffle(dex_str)
        for ability in dex_str:
            priority.add(ability)

        # then randomly-ordered abilities with racial bonus
        bonuses = [b["ability_score"]["index"] for b in self._race["ability_bonuses"]]
        random.shuffle(bonuses)
        for bonus in bonuses:
            priority.add(bonus)

        return list(priority)


    def proficient_weapons(self) -> list[dict]:
        """Get a list of all weapons usable by the Character.
        """
        weapons = []
        for proficiency in self._proficiencies:
            if proficiency["type"].lower() == "weapons":
                category = dnd.get_equipment_category(proficiency["reference"]["index"])
                if category:
                    for w in category["equipment"]:
                        weapon = dnd.get_equipment(w["index"])
                        if weapon and "damage" in weapon\
                                and all(w["index"] != weapon["index"] for w in weapons):
                            weapons.append(weapon)
                else:
                    weapon = dnd.get_equipment(proficiency["reference"]["index"])
                    if weapon and "damage" in weapon\
                            and all(w["index"] != weapon["index"] for w in weapons):
                        weapons.append(weapon)

        return weapons


    def proficient_armor(self) -> list[dict]:
        """Get a list of all armor usable by the Character.
        """
        armors = []
        for proficiency in self._proficiencies:
            if proficiency["type"].lower() == "armor":
                category = dnd.get_equipment_category(proficiency["reference"]["index"])
                if category:
                    for a in category["equipment"]:
                        armor = dnd.get_equipment(a["index"])
                        if armor and all(a["index"] != armor["index"] for a in armors):
                            armors.append(armor)
                else:
                    armor = dnd.get_equipment(proficiency["reference"]["index"])
                    if armor and all(a["index"] != armor["index"] for a in armors):
                        armors.append(armor)

        return armors


    def armor_class(self, armor: dict = None) -> int:
        """Get the effective Armor Class of the Character. Sums both armor and shields.

        `armor`: calculate Armor Class as if the Character was wearing the given armor dict.
        """
        if armor is None:
            armor = self._armor

        ac = 10 + self.ability_modifier("dex")

        if self.has_feature("monk-unarmed-defense")\
                and all(not dnd.equipment_is_shield(h) for h in self._hands):
            ac = 10 + self.ability_modifier("dex") + self.ability_modifier("wis")
        elif self.has_feature("barbarian-unarmored-defense"):
            ac = 10 + self.ability_modifier("dex") + self.ability_modifier("con")

        if armor:
            armor_ac = armor["armor_class"]["base"]
            if armor["armor_class"]["dex_bonus"]:
                if "max_bonus" in armor["armor_class"]:
                    armor_ac += min(armor["armor_class"]["max_bonus"], self.ability_modifier("dex"))
                else:
                    armor_ac += self.ability_modifier("dex")
            ac = armor_ac

        for hand in self._hands:
            if dnd.equipment_is_shield(hand):
                shield_ac = hand["armor_class"]["base"]
                if hand["armor_class"]["dex_bonus"]:
                    if "max_bonus" in hand["armor_class"]:
                        shield_ac += min(hand["armor_class"]["max_bonus"], self.ability_modifier("dex"))
                    else:
                        shield_ac += self.ability_modifier("dex")
                ac += shield_ac

        return ac


    def has_feature(self, feature_index: str) -> bool:
        """Determine if the `Character` has the given feature.
        """
        return any(f["index"] == feature_index for f in self._features)


    def weapon_modifier(self, weapon: dict) -> int:
        """Get the best ability modifier for the given weapon.

        `weapon`: weapon to find the Ability modifier for.
        """
        modifiers = dnd.get_weapon_ability(weapon)
        return max([self.ability_modifier(m) for m in modifiers])


    def weapon_dice_str(self, weapon: dict, dice: Dice) -> int:
        """Get the damage dice string for the given weapon, e.g. '2d4+2, +3'

        `weapon`: weapon dict
        `dice`: damage dice to use
        """
        modifier = self.weapon_modifier(weapon)
        return "{}{}{}, {}{}".format(
            str(dice),
            '+' if modifier >= 0 else '-',
            abs(modifier),
            '+' if modifier + self._proficiency_bonus >= 0 else '-',
            abs(modifier + self._proficiency_bonus)
        )


    def spend(self, gold: int) -> int:
        """Spend the given amount of gold.
        """
        self._gp -= gold
        return self._gp


    def earn(self, gold: int) -> int:
        """Earn the given amount of gold."""

        self._gp += gold
        return self._gp


    def equip(self, equipment: dict, replace = False, dry = False) -> list[dict]:
        """Equip the given equipment (armor or weapon). Returns unequipped equipment if applicable.

        `equipment`: equipment dict to equip.
        `replace`: True to override any currently equipped equipment if needed (this will try to
        be as smart as possible)
        `dry`: True to not actually equip, only return what would be replaced, if any.
        """
        if equipment["equipment_category"]["index"] == "weapon" or dnd.equipment_is_shield(equipment):
            # No weapons exist, just equip
            if len(self._hands) == 0:
                if not dry:
                    self._hands.append(equipment)

            # 2 light weapons, or 1 shield and 1 one-handed weapon
            elif len(self._hands) == 1:
                if ((dnd.equipment_is_shield(self._hands[0]) and dnd.equipment_is_shield(equipment)) or \
                        dnd.equipment_has_property(self._hands[0], "light") and dnd.equipment_has_property(equipment, "light")) or \
                        (not dnd.equipment_has_property(self._hands[0], "two-handed") and dnd.equipment_is_shield(equipment)) or \
                        (dnd.equipment_is_shield(self._hands[0]) and not dnd.equipment_has_property(equipment, "two-handed")):
                    if not dry:
                        self._hands.append(equipment)
                # Special case ignoring `replace`, we don't want to not have a weapon
                elif replace:
                    old_hands = copy.deepcopy(self._hands)
                    if not dry:
                        self._hands.clear()
                        self._hands.append(equipment)
                    return old_hands

            # Hands full, attempt to smartly replace current equipment
            elif len(self._hands) == 2 and replace:
                # Replace existing shield first, then the lowest damage weapon
                if dnd.equipment_is_shield(equipment):
                    min_damage_index = 0
                    min_damage = float("inf")
                    for i, hand in enumerate(self._hands):
                        if dnd.equipment_is_shield(hand):
                            min_damage_index = i
                            break
                        roll = Dice(hand["damage"]["damage_dice"]).max()
                        if (roll <= min_damage):
                            min_damage = roll
                            min_damage_index = i
                    old_hand = self._hands[min_damage_index]
                    if not dry:
                        self._hands[min_damage_index] = equipment
                    return [old_hand]

                # Equip light weapon alongside another light weapon only
                if dnd.equipment_has_property(equipment, "light"):
                    hand_to_replace = 0
                    if dnd.equipment_is_shield(self._hands[0]):
                        if dnd.equipment_has_property(self._hands[1], "light"):
                            hand_to_replace = 0
                        else:
                            hand_to_replace = 1
                    elif dnd.equipment_is_shield(self._hands[1]):
                        if dnd.equipment_has_property(self._hands[0], "light"):
                            hand_to_replace = 1
                        else:
                            hand_to_replace = 0
                    else:
                        if Dice(self._hands[0]["damage"]["damage_dice"]).max() > Dice(self._hands[1]["damage"]["damage_dice"]).max():
                            hand_to_replace = 1
                        else:
                            hand_to_replace = 0

                    old_hand = self._hands[hand_to_replace]
                    if not dry:
                        self._hands[hand_to_replace] = equipment
                    return [old_hand]

                # Equip one-handed weapon alongside shield only
                if not dnd.equipment_has_property(equipment, "two-handed"):
                    old_hands = []
                    new_hands = []
                    for hand in self._hands:
                        if dnd.equipment_is_shield(hand) and len(new_hands) < 1:
                            new_hands.append(hand)
                        else:
                            old_hands.append(hand)
                    new_hands.insert(0, equipment)
                    if not dry:
                        self._hands = new_hands
                    return old_hands

                # Anything else will replace all hands
                old_hands = copy.deepcopy(self._hands)
                if not dry:
                    self._hands.clear()
                    self._hands.append(equipment)
                return old_hands

        elif equipment["equipment_category"]["index"] == "armor":
            if (not self._armor or replace):
                old_armor = self._armor
                if not dry:
                    self._armor = equipment
                return [old_armor] if old_armor else []

        return []


    def unequip(self) -> list[dict]:
        """Unequips all equipment, returning each item in a list.
        """
        equipped = []
        for hand in self._hands:
            equipped.append(hand)

        if self._armor:
            equipped.append(self._armor)

        self._hands.clear()
        self._armor = {}
        return equipped


    def attack_options(self, max_count: int, target: Creature = None) -> list[dnd.AttackSummary]:
        """Get the list of attacks the Character can make right now.
        When possible, this function will try to provide one attack with each weapon's
        strongest attack, then to have a melee and ranged option.

        `max_count`: maximum number of attacks to return.
        """
        attacks_dict = {
            0: [],
            1: []
        }
        weapons = [hand for hand in self._hands if "damage" in hand]

        def mod_str(attack: dnd.Attack | dnd.AttackSummary, target: Creature) -> str:
            if isinstance(attack, dnd.Attack):
                if self.has_disadvantage(attack, target)\
                        or attack._damage_type in target._resistances\
                        or attack._damage_type in target._immunities:
                    return util.emojis["prompts"]["disadvantage"]
                elif attack._damage_type in target._vulnerabilities:
                    return util.emojis["prompts"]["advantage"]
            else:
                same = set([mod_str(a, target) for a in attack.get_attacks()])
                if len(same) == 1:
                    return same.pop()
            return ""

        for i, weapon in enumerate(weapons):
            modifier = self.weapon_modifier(weapon)
            two_handed = dnd.equipment_has_property(weapon, "two-handed")
            default_dice = Dice(weapon["damage"]["damage_dice"])

            # Melee weapon
            if weapon["weapon_range"].lower() == "melee":
                # One-handed attack as two-handed
                if dnd.equipment_has_property(weapon, "versatile") and len(self._hands) == 1:
                    dice = Dice(weapon["two_handed_damage"]["damage_dice"])
                    attack_weapon = dnd.AttackWeapon(
                        weapon,
                        dice,
                        weapon["damage"]["damage_type"]["index"],
                        modifier,
                        True,
                        dnd.AttackRange.MELEE
                    )
                    attack_prompt = lang.attack_character.type.choose(
                        util.emojis["attacks"]["melee"],
                        str(dice),
                        mod_str(attack_weapon, target),
                        option="melee_two"
                    )
                    attack_summary = dnd.AttackSummary([attack_weapon], attack_prompt)
                    attacks_dict[i].append(attack_summary)
                else:
                    # Regular attack
                    attack_weapon = dnd.AttackWeapon(
                        weapon,
                        default_dice,
                        weapon["damage"]["damage_type"]["index"],
                        modifier,
                        True,
                        dnd.AttackRange.MELEE
                    )
                    attack_prompt = lang.attack_character.type.choose(
                        util.emojis["attacks"]["melee"],
                        str(default_dice),
                        mod_str(attack_weapon, target),
                        option="melee_two" if two_handed else "melee_one"
                    )
                    attack_summary = dnd.AttackSummary([attack_weapon], attack_prompt)
                    attacks_dict[i].append(attack_summary)

                # Thrown attack
                if dnd.equipment_has_property(weapon, "thrown"):
                    attack_weapon = dnd.AttackWeapon(
                        weapon,
                        default_dice,
                        weapon["damage"]["damage_type"]["index"],
                        modifier,
                        True,
                        dnd.AttackRange.RANGED
                    )
                    attack_prompt = lang.attack_character.type.choose(
                        util.emojis["attacks"]["ranged"],
                        str(default_dice),
                        mod_str(attack_weapon, target),
                        option = "melee_thrown"
                    )
                    attack_summary = dnd.AttackSummary([attack_weapon], attack_prompt)
                    attacks_dict[i].append(attack_summary)
                # Improvised thrown
                else:
                    dice = Dice("1d4")
                    attack_weapon = dnd.AttackWeapon(
                        weapon,
                        dice,
                        "bludgeoning",
                        self.ability_modifier("dex"),
                        False,
                        dnd.AttackRange.RANGED
                    )
                    attack_prompt = lang.attack_character.type.choose(
                        util.emojis["attacks"]["ranged"],
                        str(dice),
                        mod_str(attack_weapon, target),
                        option = "melee_improvised"
                    )
                    attack_summary = dnd.AttackSummary([attack_weapon], attack_prompt)
                    attacks_dict[i].append(attack_summary)

            # Ranged weapon
            else:
                # Regular attack
                attack_weapon = dnd.AttackWeapon(
                    weapon,
                    default_dice,
                    weapon["damage"]["damage_type"]["index"],
                    modifier,
                    True,
                    dnd.AttackRange.RANGED
                )
                attack_prompt = lang.attack_character.type.choose(
                    util.emojis["attacks"]["ranged"],
                    str(default_dice),
                    mod_str(attack_weapon, target),
                    option="ranged_two" if two_handed else "ranged_one"
                )
                attack_summary = dnd.AttackSummary([attack_weapon], attack_prompt)
                attacks_dict[i].append(attack_summary)

                # Improvised melee attack
                dice = Dice("1d4")
                attack_weapon = dnd.AttackWeapon(
                    weapon,
                    dice,
                    "bludgeoning",
                    self.ability_modifier("str"),
                    False,
                    dnd.AttackRange.MELEE
                )
                attack_prompt = lang.attack_character.type.choose(
                    util.emojis["attacks"]["melee"],
                    str(dice),
                    mod_str(attack_weapon, target),
                    option="ranged_improvised"
                )
                attack_summary = dnd.AttackSummary([attack_weapon], attack_prompt)
                attacks_dict[i].append(attack_summary)

        combined_attacks: list[dnd.AttackSummary] = []
        for attack1 in attacks_dict[0]:
            for attack2 in attacks_dict[1]:
                attack_summary = dnd.AttackSummary.combine(attack1, attack2)
                weapon_str = " | ".join(str(w._damage_dice) for w in attack_summary.get_attacks())
                dis = mod_str(attack_summary, target)
                prompt = lang.attack_character.type.choose(
                    util.emojis["attacks"]["melee"],
                    weapon_str,
                    dis,
                    option="melee_two"
                )
                if attack_summary.get_range() == dnd.AttackRange.MIXED:
                    prompt = lang.attack_character.type.choose(
                        util.emojis["attacks"]["mixed"],
                        weapon_str,
                        dis,
                        option="mixed"
                    )
                elif attack_summary.get_range() == dnd.AttackRange.RANGED:
                    prompt = lang.attack_character.type.choose(
                        util.emojis["attacks"]["ranged"],
                        weapon_str,
                        dis,
                        option="ranged_two"
                    )

                attack_summary.set_prompt(prompt)
                combined_attacks.append(attack_summary)

        for attack in attacks_dict[0]:
            combined_attacks.append(attack)

        for attack in attacks_dict[1]:
            combined_attacks.append(attack)

        # Unarmed Strike
        unarmed_attack = dnd.AttackWeapon(
            {
                "index": "unarmed-strike",
                "name": "Unarmed Strike",
            },
            Dice("1"),
            "bludgeoning",
            self.ability_modifier("str"),
            True,
            dnd.AttackRange.MELEE
        )
        unarmed_prompt = lang.attack_character.type.choose(
            util.emojis["attacks"]["unarmed"],
            str(max(1, self.ability_modifier("str") + 1)),
            util.emojis["prompts"]["disadvantage"] * self.has_disadvantage(unarmed_attack, target),
            option = "melee_unarmed"
        )
        attack_summary = dnd.AttackSummary([unarmed_attack], unarmed_prompt)
        combined_attacks.append(attack_summary)

        # Some attacks may be virtually identical, so remove those
        unique_attacks: list[dnd.AttackSummary] = []
        unique_prompts = set()
        for attack_spell in combined_attacks:
            if attack_spell.get_prompt() not in unique_prompts:
                unique_prompts.add(attack_spell.get_prompt())
                unique_attacks.append(attack_spell)

        weapon_attacks = sorted(
            unique_attacks,
            key=lambda attack: sum([a._damage_dice.max() for a in attack.get_attacks()]),
            reverse=True
        )

        # Spell Attacks
        if self._spell_ability:
            spell_attacks = [[], []]
            spells = set()
            for level in self._spells:
                for spell in level:

                    spell_level = spell["level"]
                    # find an available spell slot of level or greater
                    while spell_level < len(self._spell_slots ) - 1\
                            and not self._spell_slots[spell_level].has_charge():
                        spell_level += 1

                    # filter non-damaging or no charge spells
                    slot = self._spell_slots[spell_level]
                    if not slot.has_charge()\
                            or spell["index"] in spells\
                            or "damage" not in spell\
                            or "damage_type" not in spell["damage"]\
                            or spell["ritual"]:
                        continue

                    # construct attack
                    spells.add(spell["index"])
                    spell_attack = dnd.AttackSpell(
                        spell,
                        self.ability_modifier(self._spell_ability),
                        slot._level,
                        self._level
                    )

                    if not spell_attacks[0] or spell_attacks[0][0]._slot_level == spell_level:
                        spell_attacks[0].append(spell_attack)
                    elif spell_attacks[0][0]._slot_level > spell_level:
                        spell_attacks[1] = spell_attacks[0]
                        spell_attacks[0] = [spell_attack]
                    else:
                        spell_attacks[1].append(spell_attack)

            # dedicated spell casters should have mostly spells,
            # others should have mostly weapons
            spell_ratio = 2/3
            if self.has_feature("spellcasting-paladin") or self.has_feature("spellcasting-ranger"):
                spell_ratio = 1/3
            spell_count = min(int(max_count * spell_ratio), len(spell_attacks[0]) + len(spell_attacks[1]))
            attacks: list[dnd.AttackSpell] = []

            if spell_count > 0 and spell_attacks[0]:
                rand_spell = random.choice(spell_attacks[0])
                spell_attacks[0].remove(rand_spell)
                attacks.append(rand_spell)
                spell_count -= 1

            while spell_count > 0 and spell_attacks[1]:
                rand_spell = random.choice(spell_attacks[1])
                spell_attacks[1].remove(rand_spell)
                attacks.append(rand_spell)
                spell_count -= 1

            while spell_count > 0 and spell_attacks[0]:
                rand_spell = random.choice(spell_attacks[0])
                spell_attacks[0].remove(rand_spell)
                attacks.append(rand_spell)
                spell_count -= 1

            attacks = sorted(attacks, key=lambda attack: attack._slot_level)
            spell_summaries = []
            for attack in attacks:
                spell_summary = dnd.AttackSummary([attack])
                prompt = lang.attack_character.type.choose(
                    util.emojis["spell_slots"][str(attack._slot_level)],
                    attack._spell["name"],
                    str(attack._damage_dice),
                    util.emojis["prompts"]["disadvantage"] * self.has_disadvantage(spell_summary, target),
                    option="spell"
                )
                spell_summary.set_prompt(prompt)
                spell_summaries.append(spell_summary)

            weapon_count = max_count - len(attacks)
            return spell_summaries + weapon_attacks[:weapon_count]

        return weapon_attacks[:max_count]


    def has_disadvantage(self, attack: dnd.Attack | dnd.AttackSummary, creature: Creature) -> bool:
        """Disadvantage when fighting flying with melee, non-flying with ranged,
        or non-specialized water weapons.

        Usually melee weapons/spells will simply not be able to be used against opponents at range,
        but for the sake of this format those attacks will just have disadvantage.
        """
        # Spells only have disadvantage if vs a flier with a melee spell
        # ranged spells are assumed to always be ok
        if isinstance(attack, dnd.AttackSpell):
            if attack._is_attack:
                return (attack._range == dnd.AttackRange.MELEE and creature.is_flier())\
                    or (attack._range == dnd.AttackRange.RANGED and not creature.is_flier())

            return attack._range == dnd.AttackRange.MELEE and creature.is_flier()

        # Attacks with weapons
        elif isinstance(attack, dnd.AttackWeapon):
            if creature.is_swimmer() and dnd.is_aquatic_weapon(attack._weapon):
                return False

            return (not creature.is_flier() and attack._range == dnd.AttackRange.RANGED)\
                or (creature.is_flier() and attack._range != dnd.AttackRange.RANGED)\
                or (creature.is_swimmer() and not dnd.is_aquatic_weapon(attack._weapon))

        # Attack summaries only are considered to have disadvantage when all attacks
        # have disadvantage in the summary
        elif isinstance(attack, dnd.AttackSummary):
            return all([self.has_disadvantage(a, creature) for a in attack.get_attacks()])


    def attack(self, other: Creature, attack_summary: dnd.AttackSummary) -> dnd.AttackResult:
        """Simulate an attack against the given Creature with the given attack, returning the result.

        `other`: `Creature` to attack
        `attack_summary`: `AttackSummary` attacks to use
        """
        total_hit = 0
        attack_hit = dnd.ActionOutcome.CRIT_FAILURE.value
        has_modifier = True

        def modify_hit(hit: int, attack: dnd.Attack) -> int:
            if (has_modifier or attack._modifier < 0) and isinstance(attack, dnd.AttackWeapon):
                hit += attack._modifier

            if attack._damage_type in other._resistances:
                hit = math.ceil(hit / 2)
                util.debug(f"{other._name} resistant to {attack._damage_type}")
            if attack._damage_type in other._vulnerabilities:
                hit *= 2
                util.debug(f"{other._name} vulnerable to {attack._damage_type}")
            if attack._damage_type in other._immunities:
                hit *= 0
                util.debug(f"{other._name} immune to {attack._damage_type}")

            # Immune damage types will still do 1 damage, since its a party of one I think this
            # should stay in since the player will not have a lot of damage types available to them
            return max(1, hit)

        # Prefer weapons that do not have disadvantage to make the primary attack
        sorted_attacks = sorted(attack_summary.get_attacks(), key=lambda a: self.has_disadvantage(a, other))
        attack_range = sorted_attacks[0]._range
        attacks = []
        self._recent_rolls["Attack Roll"] = []
        self._recent_rolls["Damage Roll"] = []

        for attack in sorted_attacks:
            prof_bonus = self._proficiency_bonus if attack._proficient else 0
            hit_check = 0
            damage_mod = 1
            is_dc = False

            # Spell DC attacks
            if isinstance(attack, dnd.AttackSpell) and attack._dc:
                is_dc = True
                dc = 8 + self.ability_modifier(self._spell_ability) + prof_bonus
                result = other.saving_throw(attack._dc["dc_type"]["index"])
                if self.has_disadvantage(attack, other):
                    util.debug("Character attacking with disadvantage.")
                    result = max(result, other.saving_throw(attack._dc["dc_type"]["index"]))
                util.debug(f"Spell Saving Throw: {result}/{dc}")
                other._recent_rolls["Saving Throw"] = result
                if result > dc:
                    damage_mod = 0.5 if attack._dc["dc_success"] == "half" else 0
            # Normal attacks
            else:
                if self.has_disadvantage(attack, other):
                    util.debug("Character attacking with disadvantage.")
                    hit_check = Dice("1d20").roll_disadvantage()
                else:
                    hit_check = Dice("1d20").roll()

            util.debug(f"Character attacking with {attack}")
            util.debug(f"Character attack roll: {hit_check} + {prof_bonus} + {attack._modifier}")
            self._recent_rolls["Attack Roll"].append(hit_check)

            # spell dc attack
            if is_dc:
                if damage_mod > 0:
                    hit = attack._damage_dice.roll(custom_mod=self.ability_modifier(self._spell_ability))
                    self._recent_rolls["Damage Roll"].append(hit)
                    total_hit += math.ceil(modify_hit(hit, attack) * damage_mod)
                    attack_range = dnd.AttackRange.combine(attack_range, attack._range)
                    attack_hit = max(attack_hit, dnd.ActionOutcome.SUCCESS.value)
                    has_modifier = False
                    attacks.append(attack)

                else:
                    attack_hit = max(attack_hit, dnd.ActionOutcome.FAILURE.value)

            # weapon attack
            else:
                # Critical success
                if hit_check == 20:
                    hit = attack._damage_dice.roll(2)
                    self._recent_rolls["Damage Roll"].append(hit)
                    total_hit += modify_hit(hit, attack)
                    attack_range = dnd.AttackRange.combine(attack_range, attack._range)
                    attack_hit = max(attack_hit, dnd.ActionOutcome.CRIT_SUCCESS.value)
                    has_modifier = False
                    attacks.append(attack)

                # Critical failure
                elif hit_check == 1:
                    attack_hit = max(attack_hit, dnd.ActionOutcome.CRIT_FAILURE.value)

                # Normal hit
                elif hit_check + prof_bonus + attack._modifier >= other.armor_class():
                    hit = attack._damage_dice.roll()
                    self._recent_rolls["Damage Roll"].append(hit)
                    total_hit += math.ceil(modify_hit(hit, attack) * damage_mod)
                    attack_range = dnd.AttackRange.combine(attack_range, attack._range)
                    attack_hit = max(attack_hit, dnd.ActionOutcome.SUCCESS.value)
                    has_modifier = False
                    attacks.append(attack)

                # Normal miss
                else:
                    attack_hit = max(attack_hit, dnd.ActionOutcome.FAILURE.value)

        # TODO: each attack should do a separate instance of damage
        attack_name = f"{attack_range.name.title()} Attack"
        if all(isinstance(a, dnd.AttackSpell) for a in attacks):
            attack_name = f"Spell Attack"
        return dnd.AttackResult(attacks, total_hit, dnd.ActionOutcome(attack_hit), attack_name)


    def death_saving_throws(self) -> bool:
        """Perform death saving throws, returning True if throws stabilize the Character.
        If the Character becomes stable, they also heal 1 hitpoint (differs from normal rules).
        """
        self._recent_rolls["Death Saving Throws"] = []
        successes = 0
        failures = 0
        while successes < 3 and failures < 3:
            roll = Dice("1d20").roll()
            self._recent_rolls["Death Saving Throws"].append(roll)
            if roll == 20:
                successes = 3
            elif roll == 1:
                failures += 2
            elif roll >= 10:
                successes += 1
            else:
                failures += 1

        if successes >= 3:
            self._hitpoints = 1

        return successes >= 3


    def add_experience(self, xp: int) -> bool:
        """Add experience to the Character, leveling up if needed.

        `exp`: experience points to add.
        """
        old_level = self._level
        self._xp += xp
        self._level = dnd.experience_level(self._xp)

        # Level up
        for i in range(old_level + 1, self._level + 1):
            prev_level_dict = dnd.get_class_level(self._class["index"], i - 1)
            level_dict = dnd.get_class_level(self._class["index"], i)
            old_max_hitpoints = self._max_hitpoints
            old_proficiency_bonus = self._proficiency_bonus

            # hitpoint increase
            self._max_hitpoints += max(1, Dice(f"1d{self._class['hit_die']}").roll() + self.ability_modifier("con"))
            # proficiency increase
            self._proficiency_bonus = level_dict["prof_bonus"]

            # ability score increase
            ability_bonuses = {}
            if "ability_score_bonuses" in level_dict:
                prev_bonus = 0
                if "ability_score_bonuses" in prev_level_dict:
                    prev_bonus = prev_level_dict["ability_score_bonuses"]
                def ability_to_increase():
                    for a in self.ability_priority():
                        if self._abilities[a] < 20:
                            return a
                    for a, score in self._abilities.items():
                        if score < 20:
                            return a
                    return None

                points_to_spend = 2 * (level_dict["ability_score_bonuses"] - prev_bonus)
                for _ in range(points_to_spend):
                    ability = ability_to_increase()
                    if ability:
                        self._abilities[ability] += 1
                        ability_bonuses.setdefault(ability, 0)
                        ability_bonuses[ability] += 1

            rewards = []
            status = "{} {} reached level {}!\n".format(
                util.emojis["experience"]["level"],
                self._name,
                i
            )

            if self._max_hitpoints > old_max_hitpoints:
                rewards.append("+{}{}".format(
                    self._max_hitpoints - old_max_hitpoints,
                    util.emojis["health"]["full"]
                ))

            for ability, increase in ability_bonuses.items():
                rewards.append("+{} {}".format(increase, ability.title()))

            if self._proficiency_bonus > old_proficiency_bonus:
                rewards.append("+{} Prof. Bonus".format(
                    self._proficiency_bonus - old_proficiency_bonus
                ))

            spells_learned, slots_gained = self.learn_spells()

            slots = []
            for i, slot in enumerate(slots_gained):
                if slot > 0:
                    slots.append("+{}{}".format(slot, util.emojis["spell_slots"][str(i)]))
            if slots:
                rewards.append(", ".join(slots))

            if spells_learned:
                spell_strs = []
                for spell in spells_learned:
                    spell_strs.append("{}{}".format(
                        util.emojis["magic_schools"][spell["school"]["index"]],
                        spell["name"]
                    ))
                rewards.append("+ " + ", ".join(spell_strs))

            client.post_status(status + "\n".join(rewards))

        return self._level > old_level


    def short_rest(self) -> int:
        """Perform a short rest.

        - Restores all hitpoints by using hit dice
        - Sometimes restores spell slots
        """
        # Regenerate hitpoints
        hitpoints_gained = 0
        while self._hitpoints < self._max_hitpoints and self._hit_dice > 0:
            roll = max(1, Dice(f"1d{self._class['hit_die']}").roll() + self.ability_modifier("con"))
            hitpoints_gained += self.heal(roll)
            self._hit_dice -= 1

        # Recharge spell slots
        # Warlock
        if self.has_feature("pact-magic"):
            for slot in self._spell_slots:
                slot.recharge()
        # Wizard
        elif self.has_feature("arcane-recovery"):
            if "arcane-recovery" not in self._used_today:
                self._used_today["arcane-recovery"] = 0

            if self._used_today["arcane-recovery"] == 0:
                recovery_count = math.ceil(self._level / 2)
                recovered = False
                # restore highest level slots first, up to level 5
                for slot in reversed(self._spell_slots[:6]):
                    if slot._current_charges < slot._max_charges and recovery_count >= slot._level:
                        recharge_count = recovery_count // slot._level
                        slots_recharged = slot.recharge(recharge_count)
                        recovery_count -= slot._level * slots_recharged
                        recovered = True
                if recovered:
                    self._used_today["arcane-recovery"] += 1

        return hitpoints_gained


    def long_rest(self):
        """Perform a long rest.

        - Restores all hitpoints and hit dice
        - Restores all spell slots
        - Resets per-day abilities/features
        """
        # regen hitpoints
        old_hitpoints = self._hitpoints
        old_hit_dice = self._hit_dice
        self._hitpoints = self._max_hitpoints
        self._hit_dice = min(self._level, self._hit_dice + math.ceil(self._level / 2))
        LongRestResult = namedtuple("LongRestResult", ["hitpoints", "hitdice"])

        # recharge spell slots
        for slot in self._spell_slots:
            slot.recharge()

        # reset any daily use features/abilities
        self._used_today = {u: 0 for u in self._used_today}

        return LongRestResult(self._hitpoints - old_hitpoints, self._hit_dice - old_hit_dice)


    def title(self) -> str:
        """Get the Character's title; including their name, race, and class.
        """
        return f"{self._name}, the {self._race['name']} {self._class['name']}"


    def emoji(self) -> str:
        """Get the emoji that represents the Character's class.
        """
        return util.emojis["classes"][self._class["index"]]


    def hand_str(self) -> str:
        """Get the string to display hand equipment.
        """
        hands = ""
        for hand in self._hands:
            if hand["equipment_category"]["index"] == "weapon":
                hands += "{} {} [{}] ".format(
                    util.emojis["equipment"]["weapon_2h"] \
                        if dnd.equipment_has_property(hand, "two-handed") \
                        else util.emojis["equipment"]["weapon_1h"],
                    hand["name"],
                    self.weapon_dice_str(hand, Dice(hand["damage"]["damage_dice"]))
                )
            else:
                hands += "{} {} ".format(
                    util.emojis["equipment"]["armor"],
                    hand["name"]
                )
        return hands


    def armor_str(self) -> str:
        """Get the string to display armor.
        """
        return "{} {} [{}]".format(
            util.emojis["equipment"]["armor"],
            self._armor["name"] if self._armor else "none",
            self.armor_class()
        )


    def bio(self) -> str:
        """Create a bio string for this Character.
        """
        bio = "{} {}".format(
            self.emoji(),
            self.title()
        )
        bio += f"\n{self.hitpoint_bar()}"
        bio += "\n{} {} ({})".format(
            util.emojis["experience"]["level"],
            self._level,
            self._xp
        )
        bio += " {}".format(
            "{} {}/{}".format(util.emojis["equipment"]["dice"], self._hit_dice, self._level)
        )
        bio += f"\n{self.hand_str()}"
        bio += f"\n{self.armor_str()}"
        spell_slots = ""
        for slot in self._spell_slots[1:]:
            if not slot.can_use():
                continue
            spell_slots += "{}{}/{} ".format(
                util.emojis["spell_slots"][str(slot._level)],
                slot._current_charges,
                slot._max_charges
            )
        if spell_slots:
            bio += f"\n{spell_slots}"
        bio += "\n{} {}".format(
            util.emojis["equipment"]["gold"],
            self._gp
        )
        bio += "\n{}".format(
            " ".join(f"{a.title()} {self._abilities[a]}" for a in self.ORDERED_ABILITY_INDEXES)
        )

        return bio


    def short_bio(self) -> str:
        """Create a death recap string for this Character.
        """
        health = "{} {}".format(
            util.emojis["health"]["full"],
            self._max_hitpoints
        )
        level = "{} {}".format(
            util.emojis["experience"]["level"],
            self._level
        )
        gold = "{} {}".format(
            util.emojis["equipment"]["gold"],
            self._gp
        )
        kills = "{} {}".format(
            util.emojis["health"]["death"],
            self._stats["kills"]
        )

        return lang.bio_character.short.choose(
            util.emojis["health"]["grave"],
            self.title(),
            health,
            level,
            gold,
            kills
        )


    def extended_bio(self) -> str:
        """Extended bio to be displayed when queried with commands.
        """
        bio = "{} {}\n".format(
            self.emoji(),
            self.title()
        )

        bio += "{} {}/{} ".format(
            util.emojis["health"]["full"],
            self._hitpoints,
            self._max_hitpoints,
        )
        bio += "{} {} ({}) ".format(
            util.emojis["experience"]["level"],
            self._level,
            self._xp
        )
        bio += "{} {}/{}\n".format(
            util.emojis["equipment"]["dice"],
            self._hit_dice,
            self._level
        )

        bio += f"{self.hand_str()}\n"

        bio += "{} {} [{}]\n".format(
            util.emojis["equipment"]["armor"],
            self._armor["name"] if self._armor else "none",
            self.armor_class()
        )

        spell_slots = ""
        for slot in self._spell_slots[1:]:
            if not slot.can_use():
                continue
            spell_slots += "{}{}/{} ".format(
                util.emojis["spell_slots"][str(slot._level)],
                slot._current_charges,
                slot._max_charges
            )
        if spell_slots:
            bio += f"{spell_slots}\n"

        bio += "{} {}\n".format(
            util.emojis["equipment"]["gold"],
            self._gp
        )

        bio += " ".join(f"{a.title()} {self._abilities[a]}" for a in self.ORDERED_ABILITY_INDEXES)
        bio += " Prof. +{}\n".format(self._proficiency_bonus)

        bio += "{} {}\n".format(
            util.emojis["prompts"]["run"],
            self._walk_speed
        )

        bio += "{} {}".format(
            util.emojis["health"]["death"],
            self._stats["kills"]
        )

        return bio


    def __str__(self) -> str:
        """String representation of the Character.
        """
        return f"""{self._name}
        {self._race['name']}
        {self._class['name']}
        {" ".join(f"{a.title()} {self._abilities[a]}" for a in self._abilities)}"""
