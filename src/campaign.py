from datetime import datetime, timezone
from enum import Enum
import math
import os
import pickle
import random
import re

import client
from dice import Dice
import dnd
from character import Character
from creature import Creature
import lang
from monster import Monster
import util


class State(Enum):
    END = 0
    BUILD = 1
    ADVENTURE = 2
    FIGHT = 3
    TOWN = 4
    SHOP = 5


# TODO: the stretch goal is for each of the functions in this class
# to be separated into node types that can be populated with behavior
# via a toml configuration
class Campaign:
    """A single Campaign, spanning the lifetime of a single character.
    A new Campaign is created upon character death.
    """

    def __init__(self, campaign_id: int = 1, version: str = "0.0.0") -> None:
        """Create a new Campaign.

        `campaign_id`: campaign ID.
        `version`: version string this campaign uses.
        """
        self._id: int = campaign_id
        self._character: Character = None
        self._monster: Monster = None
        self._start_time: datetime = None
        self._state: State = State.BUILD
        self._version = version


    def connect_handlers(self) -> int:
        """Connect event handlers.
        """
        if not client.dry:
            return client.listener.add_handler(self.command_handler)


    def start(self, result=""):
        """Start the Campaign loop.

        `result`: the message string of the last poll result.
        """
        util.info(f"STARTING CAMPAIGN #{self._id}")

        while self._state != State.END:

            if self._state == State.BUILD:
                result = self.build(result)

            elif self._state == State.ADVENTURE:
                result = self.adventure(result)

            elif self._state == State.FIGHT:
                result = self.fight(result)

            elif self._state == State.TOWN:
                result = self.town(result)

            elif self._state == State.SHOP:
                result = self.shop(result)

            if self._character:
                client.update_profile(self.bio())

            util.info(result)
            self.save()

        self.end(result)
        self.save()


    def save(self):
        """Save Campaign object to file.
        """
        with open(os.path.join(util.root, "data/save"), "wb") as file:
            pickle.dump({
                "campaign": self,
                "status_chain": client.status_chain
            }, file)


    @staticmethod
    def load():
        """Load Campaign object from file.
        """
        with open(os.path.join(util.root, "data/save"), "rb") as file:
            data = pickle.load(file)
            # Temporarily account for old save methods
            if isinstance(data, Campaign):
                return data

            campaign: Campaign = None
            for key in data:
                match key:
                    case "campaign":
                        campaign = data[key]
                    case "status_chain":
                        client.status_chain = data[key]
                    case _:
                        util.error("Unexpected data key.")

            return campaign


    def build(self, last_result="") -> str:
        """Build a new Character.
        """
        # Generate character options
        util.info("Creating character options")
        characters: list[Character] = []
        followers = list(client.get_followers())
        followers_copy = followers.copy()
        classes = dnd.get_classes()
        races = dnd.get_races()
        for _ in range(client.MAX_POLL_SIZE):
            name = ""
            # choose a unique random name from follower list if available
            while len(name) < 2 and followers:
                choice = random.choice(followers)
                followers.remove(choice)
                name = choice.split(" ")[0]
                name = "".join([c for c in name if c.isalpha()])

            if not name:
                name = random.choice(followers_copy).split(" ")[0]
                name = "".join([c for c in name if c.isalpha()])

            # try to have unique classes/races
            race = None
            clazz = None

            if races:
                race = random.choice(races)
                races.remove(race)
                race = dnd.get_race(race["index"])

            if classes:
                clazz = random.choice(classes)
                classes.remove(clazz)
                clazz = dnd.get_class(clazz["index"])

            character = Character(name, race, clazz)
            characters.append(character)

        # Poll
        util.info("Polling for character selection...")
        titles = [f"{c.emoji()} {c.title()}" for c in characters]

        prompt = f"{util.emojis['campaign']['id']} Campaign {self._id} #dnd\n\n"
        prompt += lang.create.prompt.choose()
        status = client.post_poll(prompt, last_result, titles, False)
        result = client.poll_result(status["id"], False)

        # Set character
        self._character = characters[result]
        self._character.init()
        self._start_time = datetime.now(timezone.utc)
        client.update_profile(self.bio())

        self._state = State.TOWN
        return lang.create.result.choose(self._character._name)


    def fight(self, last_result="") -> str:
        """Fight a Monster.
        """
        util.info(f"Fighting {self._monster._name}")

        attack_options = self._character.attack_options(client.MAX_POLL_SIZE - 1, self._monster)
        options = [a.get_prompt() for a in attack_options]
        options.append(f"{util.emojis['prompts']['run']} Run!")

        status = client.post_poll(self._monster.bio() + f"\n{self._monster._image_url}", last_result, options)
        result = client.poll_result(status["id"])
        attack_summary = []

        # Run
        if result == len(options) - 1:
            run_result = self._character.run(self._monster)
            monster_attack = None

            match run_result:
                # Can't get away and monster gets an attack
                case dnd.ActionOutcome.CRIT_FAILURE:
                    monster_attack = self._monster.attack(self._character)
                    attack_summary.append(lang.run_character.result.choose(option="crit_failure"))
                    if monster_attack.damage > 0:
                        attack_summary.append(lang.attack_monster.result.choose(
                            self._monster._name,
                            monster_attack.name,
                            monster_attack.damage
                        ))
                    self._state = State.FIGHT

                # May get away, but if monster attack hits, combat resumes
                case dnd.ActionOutcome.FAILURE:
                    monster_attack = self._monster.attack(self._character)
                    option = "crit_success"

                    if monster_attack.damage > 0:
                        self._state = State.FIGHT
                        option = "failure"

                    attack_summary.append(lang.run_character.result.choose(
                        self._monster._name,
                        monster_attack.name,
                        monster_attack.damage,
                        option=option
                    ))

                    if monster_attack.damage <= 0:
                        self._state = State.ADVENTURE
                        self._monster = None

                # Get away, but monster can attack
                case dnd.ActionOutcome.SUCCESS:
                    monster_attack = self._monster.attack(self._character)
                    option = "crit_success"

                    if monster_attack.damage > 0:
                        option = "success"

                    attack_summary.append(lang.run_character.result.choose(
                        self._monster._name,
                        monster_attack.name,
                        monster_attack.damage,
                        option=option
                    ))
                    self._state = State.ADVENTURE
                    self._monster = None

                # Always get away with no consequences
                case dnd.ActionOutcome.CRIT_SUCCESS:
                    attack_summary.append(lang.run_character.result.choose(option="crit_success"))
                    self._state = State.ADVENTURE
                    self._monster = None

            if monster_attack and monster_attack.damage > 0:
                excess_damage = monster_attack.damage - self._character._hitpoints
                if monster_attack and self._character.damage(monster_attack.damage):
                    if excess_damage >= self._character._max_hitpoints:
                        attack_summary.append(lang.death_character.result.choose(option="instant"))
                        self._state = State.END

                    elif self._character.death_saving_throws():
                        attack_summary.append(lang.death_character.result.choose(option="unconscious"))

                    else:
                        attack_summary.append(lang.death_character.result.choose())
                        self._state = State.END

            return "\n".join(attack_summary)

        # order attacks
        creatures = (self._character, self._monster)
        while creatures[0].get_initiative() == creatures[1].get_initiative():
            creatures[0].roll_initiative()
            creatures[1].roll_initiative()
        creatures = tuple(sorted(creatures, key=lambda c: c.get_initiative(), reverse=True))
        util.debug(f"Combat order: {creatures}")

        attack_type = attack_options[result]

        # roll in pairs until someone hits
        attack_results: list[dnd.AttackResult] = []
        while not attack_results or not any(a for a in attack_results if a.attack_hit.value > 0):
            attack_results = []
            # character attacks first
            if isinstance(creatures[0], Character):
                attack_results.append(creatures[0].attack(creatures[1], attack_type))
                attack_results.append(creatures[1].attack(creatures[0]))
            # monster attacks first
            else:
                attack_results.append(creatures[0].attack(creatures[1]))
                attack_results.append(creatures[1].attack(creatures[0], attack_type))

        self._state = State.FIGHT

        # perform creature attacks
        # if an attack kills the opposing creature, the fight ends immediately
        for i, attack in enumerate(attack_results):
            j = 1 - i
            kill = creatures[j].damage(attack.damage)
            option = "default"
            if attack.attack_hit == dnd.ActionOutcome.CRIT_SUCCESS:
                option = "crit_success"

            # character attack
            if isinstance(creatures[i], Character):
                if attack.attack_hit.value > 0:
                    attack_summary.append(lang.attack_character.result.choose(
                        attack.name,
                        creatures[j]._name,
                        attack.damage,
                        option=option
                    ))

                    for a in attack.attacks:
                        if isinstance(a, dnd.AttackSpell):
                            self._character._spell_slots[a._slot_level].use()

                if kill:
                    creatures[i]._stats["kills"] += 1
                    reward_xp = self._monster._xp
                    reward_gold = max(1, math.ceil(self._monster._challenge_rating * Dice("1d6").roll()))
                    creatures[i].add_experience(reward_xp)
                    creatures[i].earn(reward_gold)

                    attack_summary.append(lang.death_monster.result.choose(
                        self._monster._name
                    ))
                    attack_summary.append("+{}{} +{}{}".format(
                        reward_gold,
                        util.emojis["equipment"]["gold"],
                        reward_xp,
                        util.emojis["experience"]["xp"]
                    ))
                    self._state = State.ADVENTURE
                    self._monster = None
                    return "\n".join(attack_summary)
            # monster attack
            else:
                if attack.attack_hit.value > 0:
                    attack_summary.append(lang.attack_monster.result.choose(
                    creatures[i]._name,
                    attack.name,
                    attack.damage,
                    option=option
                ))

                if kill:
                    # excess damage causes instant kill
                    if abs(creatures[j]._hitpoints) >= creatures[j]._max_hitpoints:
                        attack_summary.append(lang.death_character.result.choose(option="instant"))
                        self._state = State.END
                        return "\n".join(attack_summary)

                    elif creatures[j].death_saving_throws():
                        attack_summary.append(lang.death_character.result.choose(option="unconscious"))

                    else:
                        attack_summary.append(lang.death_character.result.choose())
                        self._state = State.END
                        return "\n".join(attack_summary)

        return "\n".join(attack_summary)


    def adventure(self, last_result="") -> str:
        """Adventure beyond safety and explore.
        """
        # Generate a monster
        monster_cr = dnd.xp_to_max_cr(self._character._xp)
        monster = Monster(dnd.get_monster_lower(monster_cr))

        # Poll
        option_groups = None
        options = [
            "{} Fight!".format(
                util.emojis["prompts"]["fight"]
            ),
            "{} Avoid".format(
                util.emojis["skills"]["stealth"]
            )
        ]
        # short rest only with hit dice or if the character is a spell caster
        can_rest = self._character._hit_dice > 0\
            or self._character.has_feature("pact-magic")\
            or self._character.has_feature("arcane-recovery")
        if can_rest:
            options.append("{} Short Rest (+{} -{})".format(
                util.emojis["prompts"]["short_rest"],
                util.emojis["health"]["full"],
                util.emojis["equipment"]["dice"]
            ))
            # group rest options in case of indecision
            option_groups = [[2, 3]]
        options.append("{} Go to Town".format(
            util.emojis["prompts"]["town"]
        ))
        status = client.post_poll(lang.fight.prompt.choose(monster.short_bio()) + f"\n{monster._image_url}", last_result, options)
        result = client.poll_result(status["id"], option_groups=option_groups)

        # Fight
        if result == 0:
            mi = monster.roll_initiative()
            ci = self._character.roll_initiative()
            util.debug(f"MI: {mi}, CI: {ci}")
            self._monster = monster
            self._character._recent_rolls["Initiative"] = ci
            self._monster._recent_rolls["Initiative"] = mi
            self._state = State.FIGHT
            return lang.fight.result.choose(monster._name)
        # Avoid
        elif result == 1:
            if self._character.avoid(monster).value > 0:
                self._state = State.ADVENTURE
                return lang.avoid.result.choose(monster._name, option="success")
            else:
                mi = monster.roll_initiative()
                ci = self._character.roll_initiative()
                util.debug(f"MI: {mi}, CI: {ci}")
                self._monster = monster
                self._character._recent_rolls["Initiative"] = ci
                self._monster._recent_rolls["Initiative"] = mi
                self._state = State.FIGHT
                return lang.avoid.result.choose(monster._name, option="failure")
        # Short Rest
        elif result == 2 and can_rest:
            regained = self._character.short_rest()
            self._state = State.ADVENTURE

            if regained > 0:
                return lang.rest_short.result.choose(regained)

            return "You have no hit dice remaining."
        # Town
        elif result == 3 or (result == 2 and not can_rest):
            self._state = State.TOWN
            return lang.town.result.choose()
        else:
            raise IndexError("Polled result is greater than number of options given.")


    def town(self, last_result="") -> str:
        """Rest in a town or shop.
        """
        options = [
            "{} Adventure!".format(
                util.emojis["prompts"]["adventure"]
            ),
            "{} Shop".format(
                util.emojis["prompts"]["shop"]
            ),
            "{} Long Rest (+{} +{})".format(
                util.emojis["prompts"]["long_rest"],
                util.emojis["health"]["full"],
                util.emojis["equipment"]["dice"]
            ),
            "{} Retire (campaign ends)".format(
                util.emojis["prompts"]["retire"]
            )
        ]
        status = client.post_poll(lang.town.prompt.choose(), last_result, options)
        result = client.poll_result(status["id"], option_groups=[[0, 1, 2]])

        match result:
            # Adventure
            case 0:
                self._state = State.ADVENTURE
                return lang.adventure.result.choose()

            # Shop
            case 1:
                self._state = State.SHOP
                return lang.shop.result.choose()

            # Long Rest
            case 2:
                self._state = State.TOWN
                regained = self._character.long_rest()
                return lang.rest_long.result.choose(regained.hitpoints, regained.hitdice)

            # Retire
            # Must earn a majority of votes cast
            case 3:
                self._state = State.END
                return lang.retire.choose()

            case _:
                raise IndexError("Polled result is greater than number of options given.")


    def shop(self, last_result="") -> str:
        """Shop for new equipment.
        """
        options = []
        equipment_options = []
        weapon_options = self._character.proficient_weapons()
        armor_options = self._character.proficient_armor()

        shop_size = client.MAX_POLL_SIZE - 1
        weapon_count = 0
        if not armor_options:
            weapon_count = min(len(weapon_options), shop_size)
        else:
            weapon_count = min(len(weapon_options), random.randint(1, math.ceil(shop_size / 2)))
        armor_count = min(len(armor_options), shop_size - weapon_count)

        def net_spend(new_equipment: dict) -> int:
            unequipped = self._character.equip(new_equipment, True, True)
            sell = 0
            if unequipped:
                sell = sum([dnd.exchange(e["cost"]["unit"], e["cost"]["quantity"]) for e in unequipped]) // 2
            spend = dnd.exchange(new_equipment["cost"]["unit"], new_equipment["cost"]["quantity"])
            return spend - sell

        for _ in range(weapon_count):
            weapon = None
            cost = 0
            while not weapon and weapon_options:
                weapon = random.choice(weapon_options)
                weapon_options.remove(weapon)
                cost = net_spend(weapon)
                if cost > self._character._gp:
                    weapon = None

            if weapon:
                modifier = self._character.weapon_modifier(weapon)

                emoji = util.emojis["equipment"]["weapon_2h" if dnd.equipment_has_property(weapon, "two-handed") else "weapon_1h"]
                attributes = []
                if dnd.equipment_has_property(weapon, "two-handed"):
                    attributes.append(util.emojis["weapon_properties"]["two-handed"])
                else:
                    attributes.append(util.emojis["weapon_properties"]["one-handed"])

                if dnd.equipment_has_property(weapon, "light"):
                    attributes.append(util.emojis["weapon_properties"]["light"])
                if dnd.equipment_has_property(weapon, "thrown"):
                    attributes.append(util.emojis["weapon_properties"]["thrown"])
                if dnd.equipment_has_property(weapon, "versatile"):
                    attributes.append(util.emojis["weapon_properties"]["versatile"])

                equipment_options.append(weapon)
                options.append(
                    "{} {} [{}{}{}, {}{}] ~ {} ({:+}{})".format(
                        emoji,
                        weapon["name"],
                        weapon["damage"]["damage_dice"],
                        "+" if modifier >= 0 else "-",
                        abs(modifier),
                        "+" if modifier + self._character._proficiency_bonus >= 0 else "-",
                        abs(modifier + self._character._proficiency_bonus),
                        " ".join(attributes),
                        -cost,
                        util.emojis["equipment"]["gold"]
                    )
                )

        for _ in range(armor_count):
            armor = None
            cost = 0
            while (not armor or armor == self._character._armor) and armor_options:
                armor = random.choice(armor_options)
                armor_options.remove(armor)
                cost = net_spend(armor)
                can_carry = armor["str_minimum"] <= self._character._abilities["str"]
                if cost > self._character._gp or not can_carry:
                    armor = None

            if armor:
                ac = f"[{self._character.armor_class(armor)}]"
                if dnd.equipment_is_shield(armor):
                    ac = "[+{}] ~ {}".format(
                        armor["armor_class"]["base"],
                        util.emojis["weapon_properties"]["one-handed"]
                    )
                equipment_options.append(armor)
                options.append(
                    "{} {} {} ({:+}{})".format(
                        util.emojis['equipment']['armor'],
                        armor["name"],
                        ac,
                        -cost,
                        util.emojis["equipment"]["gold"]
                    )
                )

        if not options:
            self._state = State.TOWN
            return "The shop is empty."

        options.append(f"{util.emojis['prompts']['back']} Back to Town")
        current = util.emojis["equipment"]["inventory"]
        current += f"\n{self._character.hand_str()}"
        current += f" {self._character.armor_str()}"

        status = client.post_poll(f"{lang.shop.prompt.choose()}\n{current}", last_result, options)
        result = client.poll_result(status["id"])

        # Back to town
        if result == len(options) - 1:
            self._state = State.TOWN
            return "You left the shop."

        # Buy
        choice = equipment_options[result]
        cost = net_spend(choice)
        self._character.spend(cost)
        self._character.equip(choice, True)
        self._state = State.TOWN
        return lang.purchase.result.choose(choice["name"])


    def end(self, last_result: str = "") -> None:
        """End the campaign, resulting in a death message.
        """
        if not self._character:
            return

        util.info("Campaign ended.")
        delta = datetime.now(timezone.utc) - self._start_time
        bio = self._character.short_bio()
        bio += " {} {}".format(
            util.get_clock(delta.total_seconds()),
            util.format_duration(delta.total_seconds())
        )
        client.post_status(bio, last_result)
        if client.listener:
            client.listener._command_handlers.clear()


    def bio(self) -> str:
        """Get the string to display in the bot bio.
        """
        delta = datetime.now(timezone.utc) - self._start_time
        return "{}\n\n{} {} {} {}".format(
            self._character.bio(),
            util.emojis["campaign"]["id"],
            self._id,
            util.get_clock(delta.total_seconds()),
            util.format_duration(delta.total_seconds())
        )


    def command_handler(self, user: dict, status: dict) -> None:
        """Handle commands sent by mentions.
        """
        result = re.search(f"(^|\s|<br />|<br>)({util.config['command_prefix']}\w+)", status["content"])

        if not result:
            return

        command = result.group(2).replace(util.config["command_prefix"], "")
        util.debug(f"Received command {command}")

        match command:
            # Display character information
            case "character":
                if self._character:
                    client.command_reply(self._character.extended_bio(), status)
                else:
                    client.command_reply("Character currently unavailable.", status)

            # Display monster information
            case "monster":
                if self._monster:
                    client.command_reply(self._monster.extended_bio(), status)
                else:
                    client.command_reply("Monster currently unavailable.", status)

            # Display the rolls that ocurred
            case "rolls":
                rolls_str = ""

                if self._character and self._character.recent_rolls():
                    rolls_str += f"{self._character.emoji()} Character Rolls\n{self._character.recent_rolls()}\n\n"

                if self._monster and self._monster.recent_rolls():
                    rolls_str += f"{self._monster.emoji()} Monster Rolls\n{self._monster.recent_rolls()}"

                if rolls_str:
                    client.command_reply(rolls_str, status)
                else:
                    client.command_reply("Rolls currently unavailable.", status)

            case "spells":
                if self._character:
                    spell_str = ""
                    for i, slot in enumerate(self._character._spells):
                        if not slot:
                            continue

                        if i == 0:
                            spell_str += "Cantrips:\n"
                        else:
                            spell_str += f"Level {i}:\n"
                        spell_str += ", ".join([s["name"] for s in slot])
                        spell_str += "\n"

                    if spell_str:
                        client.command_reply(spell_str, status)
                    else:
                        client.command_reply("No spells available.", status)
                else:
                    client.command_reply("Character currently unavailable.", status)

            # Blacklist the given user from being chosen
            case "blacklist":
                if str(user["id"]) not in util.blacklist["b"]:
                    util.blacklist["b"].append(str(user["id"]))
                    util.save_configs()
                client.command_reply(lang.command_reply.blacklisted.choose(), status)

            # Whitelist the given user (removes from blacklist)
            case "whitelist":
                if str(user["id"]) in util.blacklist["b"]:
                    util.blacklist["b"].remove(str(user["id"]))
                    util.save_configs()
                client.command_reply(lang.command_reply.whitelisted.choose(), status)

            # Invalid command
            case _:
                commands = ["character", "monster", "rolls", "blacklist", "whitelist"]
                help = "Available commands:\n"
                help += ", ".join(commands)
                client.command_reply(help, status)
