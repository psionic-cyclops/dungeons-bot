import random
import util


class LangItem:

    def __init__(self, language: dict, name: str) -> None:
        """Create a new `LangItem`.

        `language`: language dict to use.
        `name`: name (index) of the item.
        """
        self.lang = language[name]
        self.options = []
        if isinstance(self.lang, dict):
            for key in self.lang:
                setattr(self, key, LangItem(self.lang, key))
        elif isinstance(self.lang, list):
            self.options = self.lang
        else:
            raise TypeError(f"Key {name} is an invalid type.")


    def choose(self, *args: str, option="default") -> str:
        """Choose a string from the `LangItem`'s options.

        `args`: arguments to format the resulting string with.
        `option`: which string category to pick.
        """
        if option in self.__dict__ and isinstance(self[option], LangItem):
            return self[option].choose(*args, option=option)

        if len(self.options) == 0:
            raise IndexError("Invalid language key.")

        return random.choice(self.options).format(*args)


    def __getitem__(self, item) -> "LangItem":
        """Overrides indexing behavior, e.g. LangItem[2]
        """
        return self.__dict__[item]


lang = util.try_load("lang.toml")

# Note: when adding a category to `lang.toml`, an entry must be added here
adventure           = LangItem(lang, "adventure")
attack_character    = LangItem(lang, "attack_character")
attack_monster      = LangItem(lang, "attack_monster")
avoid               = LangItem(lang, "avoid")
bio_campaign        = LangItem(lang, "bio_campaign")
bio_character       = LangItem(lang, "bio_character")
bio_monster         = LangItem(lang, "bio_monster")
command_reply       = LangItem(lang, "command_reply")
create              = LangItem(lang, "create")
death_character     = LangItem(lang, "death_character")
death_monster       = LangItem(lang, "death_monster")
fight               = LangItem(lang, "fight")
purchase            = LangItem(lang, "purchase")
rest_long           = LangItem(lang, "rest_long")
rest_short          = LangItem(lang, "rest_short")
retire              = LangItem(lang, "retire")
reward              = LangItem(lang, "reward")
run_character       = LangItem(lang, "run_character")
shop                = LangItem(lang, "shop")
town                = LangItem(lang, "town")
