from abc import ABC, abstractmethod
import math

from dice import Dice
import dnd
import util


class Creature(ABC):
    """Describes a Creature, being either a player Character or a Monster.
    """

    def __init__(self, name: str, max_hitpoints=1, proficiency_bonus=2) -> None:
        """Create a new `Creature` (a `Character` or `Monster`).

        `name`: name of the `Creature`.
        `max_hitpoints`: maximum hitpoints for the `Creature`.
        `proficiency_bonus`: initial proficiency bonus for the `Creature`.
        """
        super().__init__()

        # Creature name
        self._name = name

        # Maximum hitpoints
        self._max_hitpoints = 1
        self.set_max_hitpoints(max_hitpoints)
        # Current hitpoints
        self._hitpoints = 1
        self.set_hitpoints(self._max_hitpoints)
        # Initiative
        self._initiative = 0

        # Dict of ability: score
        self._abilities = {}
        # Dict of skill: bool, True if proficient
        self._skills = {}
        # Dict of saving throw: bool, True if proficient
        self._saving_throws = {}

        # List of proficiencies
        self._proficiencies: list[dict] = []
        # Proficiency bonus (based on level or CR)
        self._proficiency_bonus = proficiency_bonus

        # Set of vulnerabilities to types of damage (double damage)
        self._vulnerabilities: set[str] = set()
        # Set of resistances to types of damage (half damage)
        self._resistances: set[str] = set()
        # Set of immunities to types of damage (no damage)
        self._immunities: set[str] = set()

        # Speeds
        self._walk_speed = 0
        self._swim_speed = 0
        self._fly_speed = 0
        self._climb_speed = 0
        self._burrow_speed = 0

        if not self._abilities:
            for ability in dnd.get_abilities():
                self._abilities[ability["index"]] = 0

        if not self._saving_throws:
            for ability in dnd.get_abilities():
                self._saving_throws[ability["index"]] = 0

        if not self._skills:
            for skill in dnd.get_skills():
                self._skills[skill["index"]] = 0

        # features/abilities used today that can't be used again
        self._used_today = {}

        self._recent_rolls = {}


    def set_max_hitpoints(self, max_hitpoints: int) -> int:
        """Set the maximum amount of hitpoints the `Creature` can have.

        `max_hitpoints`: new maximum hitpoints.
        """
        self._max_hitpoints = max(1, max_hitpoints)
        return self._max_hitpoints


    def set_hitpoints(self, hitpoints: int) -> int:
        """Set current hitpoints to the given value, returning the amount gained.

        `hitpoints`: new current hitpoints, capped at `max_hitpoints`.
        """
        old_hitpoints = self._hitpoints
        self._hitpoints = min(self._max_hitpoints, hitpoints)
        return self._hitpoints - old_hitpoints


    def heal(self, heal_amount: int) -> int:
        """Heal for the given amount, returning the amount gained.

        `heal_amount`: amount of hitpoints to regain.
        """
        return self.set_hitpoints(self._hitpoints + heal_amount)


    def damage(self, damage_amount: int) -> bool:
        """Take the given amount of damage, returning True if it kills the Creature.

        `damage_amount`: amount of damage to take.
        """
        self.set_hitpoints(self._hitpoints - damage_amount)
        return self._hitpoints <= 0


    def add_proficiency(self, proficiency_index: str) -> dict:
        """Add a proficiency. If it's a skill or saving throw, add it as proficient.

        `proficiency_index`: index of the proficiency to add.
        """
        proficiency = dnd.get_proficiency(proficiency_index)

        if proficiency["type"].lower() == "skills":
            self._skills[proficiency["reference"]["index"]] = 1

        elif proficiency["type"].lower() == "saving throws":
            self._saving_throws[proficiency["reference"]["index"]] = 1

        self._proficiencies.append(proficiency)
        return proficiency


    def ability_modifier(self, ability_index: str) -> int:
        """Calculate the ability modifier for the given ability.

        `ability_index`: index of the ability to get the modifier for.
        """
        return math.floor((self._abilities[ability_index] - 10) / 2)


    def ability_check(self, ability_index: str) -> int:
        """Perform an ability check for the given ability.

        `ability_index`: index of the ability to perform a check against.
        """
        roll = Dice("1d20").roll()
        if roll == 20:
            return 20
        elif roll == 1:
            return 1

        return roll + self.ability_modifier(ability_index)


    def saving_throw(self, ability_index: str) -> int:
        """Perform a saving throw for the given ability.

        `ability_index`: index of the ability to perform a saving throw against.
        """
        roll = Dice("1d20").roll()
        if roll == 20:
            return 20
        elif roll == 1:
            return 1

        if self._saving_throws[ability_index]:
            roll += max(self._saving_throws[ability_index], self._proficiency_bonus)

        return roll + self.ability_modifier(ability_index)


    def skill_check(self, skill_index: str) -> int:
        """Perform a skill check for the given skill.

        `skill_index`: index of the skill to perform a check against.
        """
        roll = Dice("1d20").roll()
        if roll == 20:
            return 20
        elif roll == 1:
            return 1

        skill_ability = dnd.get_skill(skill_index)["ability_score"]["index"]
        if self._skills[skill_index]:
            roll += max(self._skills[skill_index], self._proficiency_bonus)

        return roll + self.ability_modifier(skill_ability)


    def ability_passive_check(self, ability_index: str) -> int:
        """Perform a passive ability check.

        `ability_index`: index of the ability to passive check against.
        """
        return 10 + self.ability_modifier(ability_index)


    def skill_passive_check(self, skill_index) -> int:
        """Perform a passive skill check.

        `skill_index`: index of the skill to passive check against.
        """
        base = 10
        skill_ability = dnd.get_skill(skill_index)["ability_score"]["index"]
        if self._skills[skill_index]:
            base += max(self._skills[skill_index], self._proficiency_bonus)

        return base + self.ability_modifier(skill_ability)


    def roll_initiative(self) -> int:
        """Rolls initiative for the Creature, setting `_initiative` and returning the value.
        """
        initiative = self.ability_check("dex")
        self._initiative = initiative
        return self._initiative


    def get_initiative(self) -> int:
        """Get the Creature's initiative.
        """
        return self._initiative


    def hitpoint_bar(self) -> str:
        """Get the hitpoint bar for the Creature, with 1 heart representing 1/10 hitpoints.
        """
        full = util.emojis["health"]["full"]
        empty = util.emojis["health"]["empty"]
        full_count = max(0, self._hitpoints)
        empty_count = self._max_hitpoints - full_count

        if self._max_hitpoints > 10:
            full_count = max(0, math.ceil((self._hitpoints / self._max_hitpoints) * 10))
            empty_count = 10 - full_count

        return "{}{} {}/{}".format(
            full * full_count,
            empty * empty_count,
            self._hitpoints,
            self._max_hitpoints
        )


    def is_flier(self) -> bool:
        """Determine if the Creature is a flying creature or not.
        A creature is considered flying if its fly speed is the greatest of its speeds.
        """
        return self._fly_speed > self._walk_speed and self._fly_speed > self._swim_speed


    def is_swimmer(self) -> bool:
        """Determine if the Creature is a swimming creature or not.
        A creature is considered a swimmer if its swim speed is the greatest of its speeds.
        """
        return self._swim_speed > self._walk_speed and self._swim_speed > self._fly_speed


    def run(self, other: "Creature") -> dnd.ActionOutcome:
        """Simulate an attempt to run from the other creature. Walk, Fly, and Swim move speeds
        are averaged and used together for the purposes of this function.

        `other`: `Creature` to try to run from.
        """
        non_zero = [speed for speed in [self._walk_speed, self._fly_speed, self._swim_speed] if speed != 0]
        self_move_speed = sum(non_zero) / len(non_zero) if non_zero else 0
        non_zero = [speed for speed in [other._walk_speed, other._fly_speed, other._swim_speed] if speed != 0]
        other_move_speed = sum(non_zero) / len(non_zero) if non_zero else 0

        diff = math.trunc((self_move_speed - other_move_speed) / 4)
        roll = max(self.skill_check("athletics"), self.skill_check("acrobatics"))
        other_roll = min(other.skill_check("athletics"), other.skill_check("acrobatics"))

        util.debug(f"Attempting to run {roll}+{diff}/{other_roll}")
        self._recent_rolls["Run"] = roll
        other._recent_rolls["Run"] = other_roll

        if roll == 20 or other_roll == 1:
            return dnd.ActionOutcome.CRIT_SUCCESS
        elif roll == 1 or other_roll == 20:
            return dnd.ActionOutcome.CRIT_FAILURE
        elif roll + diff >= other_roll:
            return dnd.ActionOutcome.SUCCESS

        return dnd.ActionOutcome.FAILURE


    def avoid(self, other: "Creature") -> dnd.ActionOutcome:
        """Avoid another `Creature`, performing a Stealth check against the `other`'s passive Perception check.

        `other`: the `Creature` to try to avoid.
        """
        perception_check = other.skill_passive_check("perception")

        successes = 0
        failures = 0
        while successes < 3 and failures < 3:
            stealth_check = self.skill_check("stealth")
            if stealth_check == 20:
                successes += 3
            elif stealth_check == 1:
                failures += 2
            elif stealth_check > perception_check:
                successes += 1
            else:
                failures += 1

        return dnd.ActionOutcome.SUCCESS if successes >= 3 else dnd.ActionOutcome.FAILURE


    def recent_rolls(self) -> str:
        """Get most recent rolls for the `Creature` as a string.
        """
        rolls = []
        for key, value in self._recent_rolls.items():
            if not value:
                continue
            if isinstance(value, list):
                rolls.append(f"{key.title()}: {', '.join([str(v) for v in value])}")
            else:
                rolls.append(f"{key.title()}: {value}")
        return "\n".join(rolls)


    @abstractmethod
    def armor_class(self, armor: dict = None) -> int:
        """Get the Armor Class of the Creature.
        If `armor` is provided, AC is calculated as if using it (only on a Character).

        `armor`: armor to calculate the Armor Class with, instead of existing armor.
        """
        raise NotImplementedError


    @abstractmethod
    def attack(self, other: "Creature") -> dnd.AttackResult:
        """Attack the `other` Creature, returning the damage to be done.
        This does not actually inflict damage, only calculates an attack against `other`.

        `other`: `Creature` to simulate an attack against.
        """
        raise NotImplementedError


    @abstractmethod
    def emoji(self) -> str:
        """Get the emoji associated with the Creature.
        """
        raise NotImplementedError


    @abstractmethod
    def bio(self) -> str:
        """Get a long description of the Creature. Generally seen when directly interacting with
        the Creature.
        """
        raise NotImplementedError


    @abstractmethod
    def short_bio(self) -> str:
        """Get a short description of the Creature. Generally seen as a preview before interacting
        with the Creature.
        """
        raise NotImplementedError
