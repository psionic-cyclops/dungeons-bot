# Dungeons Bot
# Copyright (C) 2023  ASTRELION

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import signal
import sys
from time import sleep

from campaign import Campaign, State
import client
import dnd
import util


def cancel(signal, frame):
    """Intercept for ctrl-c.
    """
    util.warn("Forcibly exiting...")
    client.post_status("Campaign forcibly stopped.")
    client.close()
    sys.exit(0)


def main():
    """Main :)
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dry", "-d",
        help="Do not actually make posts.",
        action="store_true"
    )
    parser.add_argument(
        "--no-save", "-n",
        help="Do not perform or load saves.",
        action="store_true"
    )
    parser.add_argument(
        "--verbose", "-v",
        help="Set log level to DEBUG.",
        action="store_true"
    )
    parser.add_argument(
        "--quiet", "-q",
        help="Set log level to NONE.",
        action="store_true"
    )
    args = parser.parse_args()

    signal.signal(signal.SIGINT, cancel)

    if args.quiet:
        util.logger.setLevel(0)
    elif args.verbose:
        util.logger.setLevel(util.logging.DEBUG)

    dnd.reset_cache()
    campaign: Campaign = None
    campaigns = 0
    client.dry = args.dry
    if not args.dry:
        client.init_client()

    util.info("Staring epic...")

    if not args.no_save:
        try:
            campaign = Campaign.load()
            campaigns = campaign._id
            if campaign._version != util.config["_version"]:
                campaign = None
                util.warn("Loaded save from file, but it's outdated, discarding.")
            else:
                util.info("Loaded save from file, resuming.")
                # If the last campaign ended, just start a new one
                if campaign._state == State.END:
                    campaign = None
        except:
            campaign = None
            util.warn("Unable to load a save, starting fresh.")

    client.start_tips()

    while True:
        dnd.reset_cache()

        if not campaign:
            campaigns += 1
            campaign = Campaign(campaigns, util.config["_version"])
            campaign.connect_handlers()
            campaign.start()
        else:
            campaign.connect_handlers()
            campaign.start(util.emojis["prompts"]["resume"])

        campaign = None

        if not args.dry:
            sleep(util.config["death_duration"])


if __name__ == "__main__":
    main()
