import random

from creature import Creature
from dice import Dice
import dnd
import lang
import util
import copy
import math


class Monster(Creature):
    """
    Describes a Monster, a hostile Creature.
    """

    def __init__(self, monster: dict) -> None:
        """Create a new `Monster`.

        `monster`: monster dict to create the `Monster` from.
        """
        super().__init__(monster["name"], Dice(monster["hit_points_roll"]).roll())
        self._monster = monster

        self._xp = self._monster["xp"]
        self._challenge_rating = self._monster["challenge_rating"]
        self._image_url = dnd.get_monster_image_link(self._monster)

        abilities = dnd.get_abilities()
        for a in abilities:
            ability = dnd.get_ability(a["index"])
            self._abilities[a["index"]] = self._monster[ability["full_name"].lower()]

        for p in self._monster["proficiencies"]:
            proficiency = self.add_proficiency(p["proficiency"]["index"])
            if proficiency["type"].lower() == "skills":
                self._skills[proficiency["reference"]["index"]] = p["value"]

            elif proficiency["type"].lower() == "saving throws":
                self._saving_throws[proficiency["reference"]["index"]] = p["value"]

        self._proficiency_bonus = dnd.cr_proficiency(self._challenge_rating)

        self._vulnerabilities.update(self._monster["damage_vulnerabilities"])
        self._resistances.update(self._monster["damage_resistances"])
        self._immunities.update(self._monster["damage_immunities"])

        # List of action names that are recharging
        self._recharging: set[str] = set()

        # Speeds
        if "walk" in self._monster["speed"]:
            self._walk_speed = int(self._monster["speed"]["walk"].split(" ")[0])
        if "swim" in self._monster["speed"]:
            self._swim_speed = int(self._monster["speed"]["swim"].split(" ")[0])
        if "fly" in self._monster["speed"]:
            self._fly_speed = int(self._monster["speed"]["fly"].split(" ")[0])
        if "climb" in self._monster["speed"]:
            self._climb_speed = int(self._monster["speed"]["climb"].split(" ")[0])
        if "burrow" in self._monster["speed"]:
            self._burrow_speed = int(self._monster["speed"]["burrow"].split(" ")[0])


    def armor_class(self, armor: dict = None) -> int:
        """Get the Armor Class of the `Monster`.

        `armor`: ignored for `Monster`s.
        """
        ac = 0
        for armor in self._monster["armor_class"]:
            if armor["type"] == "armor" or armor["type"] == "dex" or armor["type"] == "natural":
                ac = max(ac, armor["value"])

        return ac


    def emoji(self) -> str:
        """Get the emoji associated with this `Monster`, based on type.
        """
        if self._monster["type"] in util.emojis["monster_types"]:
            return util.emojis["monster_types"][self._monster["type"]]

        return ""


    def short_bio(self) -> str:
        """Get the short description, usually displayed before interacting.
        """
        status_effects = []
        if self.is_flier():
            status_effects.append(util.emojis["attacks"]["flier"])
        if self.is_swimmer():
            status_effects.append(util.emojis["attacks"]["swimmer"])

        dice = Dice(self._monster["hit_points_roll"])

        return lang.bio_monster.short.choose(
            f"{self.emoji()} {self._name}",
            f"{util.emojis['health']['full']} {max(1, dice.min())}-{dice.max()}",
            " ".join(status_effects),
            f"{util.emojis['equipment']['challenge_rating']}{util.float_to_frac(self._challenge_rating)}",
            f"{util.emojis['experience']['xp']} {self._xp}"
        )


    def bio(self) -> str:
        """Get the long description, usually displayed while interacting.
        """
        status_effects = []
        if self.is_flier():
            status_effects.append(util.emojis["attacks"]["flier"])
        if self.is_swimmer():
            status_effects.append(util.emojis["attacks"]["swimmer"])

        return lang.bio_monster.long.choose(
            f"{self.emoji()} {self._name}",
            f"{self.hitpoint_bar()}",
            f"{util.emojis['equipment']['armor']} {self.armor_class()}",
            " ".join(status_effects),
            f"{util.emojis['equipment']['challenge_rating']}{util.float_to_frac(self._challenge_rating)}",
            f"{util.emojis['experience']['xp']} {self._xp}"
        )


    def extended_bio(self) -> str:
        """Extended bio to be displayed when queried with commands.
        """
        bio = "{} {}\n".format(
            self.emoji(),
            self._name
        )

        bio += "{} {}/{} ".format(
            util.emojis["health"]["full"],
            self._hitpoints,
            self._max_hitpoints
        )
        bio += "{} {} ".format(
            util.emojis["equipment"]["armor"],
            self.armor_class()
        )
        status_effects = []
        if self.is_flier():
            status_effects.append(util.emojis["attacks"]["flier"])
        if self.is_swimmer():
            status_effects.append(util.emojis["attacks"]["swimmer"])
        bio += " ".join(status_effects)

        if self._vulnerabilities:
            bio += "\n{} {}".format(
                util.emojis["prompts"]["advantage"],
                ", ".join([v.title() for v in self._vulnerabilities])
            )

        if self._resistances:
            bio += "\n{} {}".format(
                util.emojis["prompts"]["disadvantage"],
                ", ".join([r.title() for r in self._resistances])
            )

        if self._immunities:
            bio += "\n{} {}".format(
                util.emojis["prompts"]["negate"],
                ", ".join([i.title() for i in self._immunities])
            )

        if self._monster["actions"]:
            bio += "\n{} {}".format(
                util.emojis["attacks"]["melee"],
                ", ".join([a["name"] for a in self._monster["actions"]])
            )

        bio += "\n{} {} ".format(
            util.emojis["equipment"]["challenge_rating"],
            util.float_to_frac(self._challenge_rating)
        )
        bio += "\n{} {}".format(
            util.emojis["experience"]["xp"],
            self._xp
        )

        return bio


    def attack(self, other: Creature, action: dict = None) -> dnd.AttackResult:
        """Simulate an attack against another Creature. Non-damaging actions are ignored where possible,
        doing 1 damage when not.

        `other`: `Creature` to attack.
        `action`: action dict to use for the attack.
        """
        if "actions" not in self._monster or not self._monster["actions"]:
            return dnd.AttackResult([], 0, dnd.ActionOutcome.NONE, "")

        # Choose a damaging action, if present
        if action is None:
            actions_copy: list[dict] = copy.deepcopy(self._monster["actions"])
            action = None #random.choice(actions_copy)
            while not action and actions_copy:
                action = random.choice(actions_copy)
                actions_copy.remove(action)
                if "damage" not in action and "options" not in action and "multiattack_type" not in action:
                    action = None
                    continue

                if "usage" in action:
                    if action["name"] in self._recharging:
                        if action["usage"]["type"] == "recharge on roll":
                            recharge = Dice(action["usage"]["dice"])
                            if recharge.roll() < action["usage"]["min_value"]:
                                action = None
                        else:
                            action = None
                    else:
                        self._recharging.add(action["name"])

        if not action:
            action = random.choice(self._monster["actions"])
            if action["name"] in self._recharging:
                return dnd.AttackResult(0, dnd.ActionOutcome.NONE, "")

        attacks = [action]
        # Attacks from multiattack
        if "multiattack_type" in action:
            attacks.clear()

            def add_attacks(actions_arr: list[dict]):
                for attack in actions_arr:
                    for a in self._monster["actions"]:
                        if a["name"] == attack["action_name"]:
                            attack_count = attack["count"]

                            if isinstance(attack_count, str):
                                # Hydra -.-
                                if attack_count == "Number of Heads":
                                    attack_count = 5
                                # Dice rolls
                                elif Dice.is_dice(attack_count):
                                    attack_count = Dice(attack_count).roll()

                            attacks.extend([a] * attack_count)
                            break

            if action["multiattack_type"] == "actions":
                add_attacks(action["actions"])

            elif action["multiattack_type"] == "action_options":
                amount = action["action_options"]["choose"]
                options = action["action_options"]["from"]["options"]
                for _ in range(amount):
                    choice = random.choice(options)
                    if choice["option_type"] == "multiple":
                        add_attacks(choice["items"])
                    else:
                        add_attacks([choice])
        # Attack from options
        elif "options" in action:
            attacks.clear()
            amount = action["options"]["choose"]
            options = action["options"]["from"]["options"]
            for _ in range(amount):
                attacks.append(random.choice(options))

        attack_hit = dnd.ActionOutcome.CRIT_FAILURE.value
        total_hit = 0
        attacks_hit = []
        self._recent_rolls["Attack Roll"] = []
        self._recent_rolls["Damage Roll"] = []
        self._recent_rolls["Other Saving Throw"] = []

        util.debug(f"Monster attacking with {[a['name'] for a in attacks]}")

        for attack in attacks:
            damage_dice: list[Dice] = []
            bonus = 0
            dc_type = ""
            dc_value = 0
            dc_modifier = 1

            if "damage" in attack:
                # damage
                for damage in attack["damage"]:
                    if "choose" in damage:
                        for _ in range(damage["choose"]):
                            damage_dice.append(Dice(random.choice(damage["from"]["options"])["damage_dice"]))
                    else:
                        damage_dice.append(Dice(damage["damage_dice"]))
                # attack dc
                if "dc" in attack:
                    dc_type = attack["dc"]["dc_type"]["index"]
                    dc_value = attack["dc"]["dc_value"]
                    dc_modifier = 0.5 if attack["dc"]["success_type"] == "half" else 0
                # to hit bonus
                if "attack_bonus" in attack:
                    bonus = attack["attack_bonus"]
            else:
                damage_dice.append(Dice("1"))

            # Saving throw attack
            if dc_type:
                throw = other.saving_throw(dc_type)
                self._recent_rolls["Other Saving Throw"].append(throw)
                util.debug(f"Monster DC Roll: {throw}/{dc_value}")

                damage = 0
                for die in damage_dice:
                    roll = die.roll()
                    damage += roll
                    self._recent_rolls["Damage Roll"].append(roll)

                if dc_value >= throw:
                    total_hit += max(1, damage)
                    attack_hit = max(attack_hit, dnd.ActionOutcome.SUCCESS.value)
                    attacks_hit.append(attack)
                else:
                    total_hit += math.ceil(max(1, damage) * dc_modifier)
                    if dc_modifier > 0:
                        attack_hit = max(attack_hit, dnd.ActionOutcome.SUCCESS.value)
                        attacks_hit.append(attack)
                    else:
                        attack_hit = max(attack_hit, dnd.ActionOutcome.FAILURE.value)

            # Normal attack
            else:
                hit_check = Dice("1d20").roll()
                self._recent_rolls["Attack Roll"].append(hit_check)
                util.debug(f"Monster Attack roll: {hit_check} + {bonus}")

                # Critical success
                if hit_check == 20:
                    for die in damage_dice:
                        roll = max(1, die.roll(2))
                        total_hit += roll
                        self._recent_rolls["Damage Roll"].append(roll)
                    attack_hit = max(attack_hit, dnd.ActionOutcome.CRIT_SUCCESS.value)
                    attacks_hit.append(attack)

                # Critical failure
                elif hit_check == 1:
                    attack_hit = max(attack_hit, dnd.ActionOutcome.CRIT_FAILURE.value)

                # Normal attack
                elif hit_check + bonus >= other.armor_class():
                    for die in damage_dice:
                        roll = max(1, die.roll())
                        total_hit += roll
                        self._recent_rolls["Damage Roll"].append(roll)
                    attack_hit = max(attack_hit, dnd.ActionOutcome.SUCCESS.value)
                    attacks_hit.append(attack)

                # Miss
                else:
                    attack_hit = max(attack_hit, dnd.ActionOutcome.FAILURE.value)

        return dnd.AttackResult(attacks_hit, total_hit, dnd.ActionOutcome(attack_hit), action["name"])
