# Contributing

The project is mostly maintained by one person and undergoes frequent system changes and rewrites. I would suggest only making low-scope contributions for the time being.

Consider joining the [Dev room in the Matrix space](https://matrix.to/#/#dungeonsdev:matrix.org) if you make frequent contributions or wish to discuss development.

1. Fork the repository
2. Clone your fork
3. Create a branch for your changes, based on `develop`
4. Make your changes
5. Run tests (these tests do not offer good coverage, make sure to test yourself locally, ideally with a test Mastodon account)
    ```shell
    python3 -m pytest
    ```
6. Commit and push your changes
7. [Submit a Merge Request](https://gitlab.com/ASTRELION/dungeons-bot/-/merge_requests/new) (same as a Pull Request (PR)) to the `develop` branch of the original repository
